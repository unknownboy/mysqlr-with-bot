var sessionUtils = require('./../services/sessionUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var redisUtils = require('./../services/redisUtils');
var mysqlUtils = require('./../services/mysqlUtils');
const { execSync  } = require('child_process');

var util = require('util');
var fs = require('fs');
var GCM = require('node-crypto-gcm').GCM;
var gcm = new GCM('t86GvATWQV6S', {algorithm: 'aes-256-gcm',
    saltLenght: 123,
    pbkdf2Rounds: 1000,
    pbkdf2Digest: 'sha512'});
var nodemailer = require('nodemailer');

module.exports = {
    terminalDump:function *(next) {
        var dump = this.params.dump;
        console.log(dump);

        try {
            var res = yield databaseUtils.executeQuery(util.format('create database m' + this.currentUser.id));
        }catch (e) {

        }

        var queries = fs.readFileSync('static/static/uploads/'+dump+'.sql','utf-8');
        res = yield mysqlUtils.executeDump(util.format(queries),'m'+this.currentUser.id);

        yield this.render('terminal',{

        });

    },
    terminal  : function *(next) {
        var pid = this.query.pid;
        try {
            var res = yield databaseUtils.executeQuery(util.format('create database m' + this.currentUser.id));
        }catch (e) {

        }
        var user = this.currentUser;
        user['pid']=pid;
        sessionUtils.updateUserInSession(user,this.cookies);
        console.log(this.currentUser);

        res = yield databaseUtils.executeQuery(util.format('select dump from practical where id="%s"',pid));
        console.log(res);

        var queries = fs.readFileSync('static/static/uploads/'+res[0].dump,'utf-8');
        res = yield mysqlUtils.executeDump(util.format(queries),'m'+this.currentUser.id);

        yield this.render('terminal',{

        });
    },
    doQuery : function *(next) {
        console.log(this.currentUser);


        try {
            var myquery = this.request.body.query;
            console.log(myquery,this.request.body.qid);
            var type = myquery.split(' ');
            var res;
            if ( type[0].toLowerCase() === 'save' ){
                if ( type.length==2 ){
                    // name given
                    var name = 'm'+this.currentUser.id+'_'+new Date().toString().substring(0,24).replace(/\s/g,'_').replace(/:/g,'-');
                    var databasename = 'm'+this.currentUser.id;
                    var res =execSync('mysqldump -uroot -proot '+databasename+'>static/static/uploads/'+name+'.sql');
                    res = yield databaseUtils.executeQuery(util.format('insert into mydump(dump,uid,tag) values("%s","%s","%s")',name,this.currentUser.id,type[1]));
                } else{
                    // name not given
                    var name = 'm'+this.currentUser.id+'_'+new Date().toString().substring(0,24).replace(/\s/g,'_').replace(/:/g,'-');
                    var databasename = 'm'+this.currentUser.id;
                    var res = execSync('mysqldump -uroot -proot '+databasename+'>static/static/uploads/'+name+'.sql');
                    res = yield databaseUtils.executeQuery(util.format('insert into mydump(dump,uid,tag) values("%s","%s","%s")',name,this.currentUser.id,''));
                }
                this.body='Saved !!';
            } else {
                console.log(this.currentUser.pid, this.request.body.qid, this.currentUser);


                if (this.currentUser.pid && this.request.body.qid != 0) {

                    var multipletype = yield databaseUtils.executeQuery(util.format('select * from practical where id="%s"', this.currentUser.pid));
                    multipletype = multipletype[0].multiple;
                    if (multipletype == 1) multipletype = true; else multipletype = false;


                    try{
                    var response;

                    var flag = 1;
                    if (type[0].toLowerCase() === 'create' && type[1].toLowerCase() === 'table') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id

                        response = 'SuccessFully Submitted !!';
                        var name = 'p' + this.currentUser.id;
                        var databasename = 'm' + this.currentUser.id;
                        var res2 = execSync('mysqldump -uroot -proot ' + databasename + '>static/static/uploads/' + name + '.sql');

                        var file1 = fs.readFileSync('static/static/uploads/' + name + '.sql', 'utf-8').toString().split('\n');
                        var file2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8').toString().split('\n');

                        console.log(file1);
                        console.log(file2);

                        var maxlen = file1.length;
                        if (file2.length > file1.length) maxlen = file2.length;


                        try {

                            for (var i = 3; i < maxlen - 2; ++i)
                                if (!(file2[i] === file1[i])) {
                                    flag = 0;
                                    break;
                                }

                            yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, flag));

                        } catch (e) {
                            if (multipletype) {
                                yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', flag, this.currentUser.id, this.request.body.qid));
                            }
                        }

                        fs.unlinkSync('static/static/uploads/' + name + '.sql');
                        this.body = response;

                    }
                    else if (type[0].toLowerCase() === 'insert' && type[1].toLowerCase() === 'into') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        response = 'SuccessFully Submitted !!';

                        var name = 'p' + this.currentUser.id;
                        var databasename = 'm' + this.currentUser.id;
                        var res2 = execSync('mysqldump -uroot -proot ' + databasename + '>static/static/uploads/' + name + '.sql');
                        var file1 = fs.readFileSync('static/static/uploads/' + name + '.sql', 'utf-8').toString().split('\n');
                        var file2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8').toString().split('\n');

                        console.log(file1);
                        console.log(file2);

                        var maxlen = file1.length;
                        if (file2.length > file1.length) maxlen = file2.length;

                        try {

                            for (var i = 3; i < maxlen - 2; ++i)
                                if (!(file2[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00') === file1[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00'))) {
                                    flag = 0;
                                    break;
                                }
                            yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, flag));

                        } catch (e) {
                            if (multipletype) {
                                yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', flag, this.currentUser.id, this.request.body.qid));
                            }
                        }

                        fs.unlinkSync('static/static/uploads/' + name + '.sql');
                    }
                    else if (type[0].toLowerCase() === 'update') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        response = 'SuccessFully Submitted !!';

                        var name = 'p' + this.currentUser.id;
                        var databasename = 'm' + this.currentUser.id;
                        var res2 = execSync('mysqldump -uroot -proot ' + databasename + '>static/static/uploads/' + name + '.sql');
                        var file1 = fs.readFileSync('static/static/uploads/' + name + '.sql', 'utf-8').toString().split('\n');
                        var file2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8').toString().split('\n');

                        console.log(file1);
                        console.log(file2);

                        var maxlen = file1.length;
                        if (file2.length > file1.length) maxlen = file2.length;

                        try {

                            for (var i = 3; i < maxlen - 2; ++i)
                                if (!(file2[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00') === file1[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00'))) {
                                    flag = 0;
                                    break;
                                }
                            yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, flag));

                        } catch (e) {
                            if (multipletype) {
                                yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', flag, this.currentUser.id, this.request.body.qid));
                            }
                        }

                        fs.unlinkSync('static/static/uploads/' + name + '.sql');
                    }
                    else if (type[0].toLowerCase() === 'drop' && type[1].toLowerCase() === 'table') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        response = 'SuccessFully Submitted !!';

                        var name = 'p' + this.currentUser.id;
                        var databasename = 'm' + this.currentUser.id;
                        var res2 = execSync('mysqldump -uroot -proot ' + databasename + '>static/static/uploads/' + name + '.sql');
                        var file1 = fs.readFileSync('static/static/uploads/' + name + '.sql', 'utf-8').toString().split('\n');
                        var file2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8').toString().split('\n');

                        console.log(file1);
                        console.log(file2);

                        var maxlen = file1.length;
                        if (file2.length > file1.length) maxlen = file2.length;

                        try {

                            for (var i = 3; i < maxlen - 2; ++i)
                                if (!(file2[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00') === file1[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00'))) {
                                    flag = 0;
                                    break;
                                }
                            yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, flag));

                        } catch (e) {
                            if (multipletype) {
                                yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', flag, this.currentUser.id, this.request.body.qid));
                            }
                        }

                        fs.unlinkSync('static/static/uploads/' + name + '.sql');
                    }
                    else if (type[0].toLowerCase() === 'show' && type[1].toLowerCase() === 'databases')
                        response = 'Error: You Do Not Have Access To Create, See And Drop Database';
                    else if (type[0].toLowerCase() === 'drop' && type[1].toLowerCase() === 'database')
                        response = 'Error: You Do Not Have Access To Create, See And Drop Database';
                    else if (type[0].toLowerCase() === 'create' && type[1].toLowerCase() === 'database')
                        response = 'Error: You Do Not Have Access To Create, See And Drop Database';
                    else if (type[0].toLowerCase() === 'show' && type[1].toLowerCase() === 'tables') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id

                        try {
                            var res2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid, 'utf-8');

                            console.log(JSON.stringify(res).toString().replace(/Tables_in_[a-zA-Z0-9]*/g, 'Tables'), res2.toString().replace(/Tables_in_[a-zA-Z0-9]*/g, 'Tables'));

                            if (JSON.stringify(res).toString().replace(/Tables_in_[a-zA-Z0-9]*/g, 'Tables') === res2.toString().replace(/Tables_in_[a-zA-Z0-9]*/g, 'Tables')) {
                                try {
                                    yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 1));
                                    response = 'SuccessFully Submitted !!';
                                } catch (e) {
                                    if (multipletype) {
                                        response = 'SuccessFully Submitted !!';
                                        yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 1, this.currentUser.id, this.request.body.qid));
                                    } else {
                                        response = 'Already Submitted !!';
                                    }
                                }
                            } else {
                                try {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 0));

                                } catch (e) {
                                    response = 'Already Submitted !!';
                                }
                            }
                        } catch (e) {
                            try {
                                yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 0));
                            } catch (e) {
                                if (multipletype) {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 0, this.currentUser.id, this.request.body.qid));
                                } else {
                                    response = 'Already Submitted !!';
                                }
                            }
                        }
                    }
                    else if (type[0].toLowerCase() === 'select') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        var res2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8');
                        if (res2 === '') res2 = '[]';

                        if (JSON.stringify(res).toString().replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+/g, '0000-00-00T00:00:00') === res2.toString().replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+/g, '0000-00-00T00:00:00')) {
                            try {
                                yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 1));
                                response = 'SuccessFully Submitted !!';
                            } catch (e) {
                                if (multipletype) {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 1, this.currentUser.id, this.request.body.qid));
                                } else
                                    response = 'Already Submitted !!';
                            }
                        } else {
                            console.log(res, res2, JSON.stringify(res).toString().replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+/g, '0000-00-00T00:00:00'), res2.toString().replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+/g, '0000-00-00T00:00:00'));

                            try {
                                yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 0));
                                response = 'SuccessFully Submitted !!';
                            } catch (e) {
                                if (multipletype) {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 0, this.currentUser.id, this.request.body.qid));
                                } else
                                    response = 'Already Submitted !!';
                            }
                        }
                    }
                    else if (type[0].toLowerCase() === 'desc') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        var res2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid, 'utf-8');

                        if (JSON.stringify(res).toString() === res2.toString()) {
                            try {
                                yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 1));
                                response = 'SuccessFully Submitted !!';
                            } catch (e) {
                                if (multipletype) {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 0, this.currentUser.id, this.request.body.qid));
                                } else
                                    response = 'Already Submitted !!';
                            }
                        } else {
                            try {
                                yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 0));
                                response = 'SuccessFully Submitted !!';
                            } catch (e) {
                                if (multipletype) {
                                    response = 'SuccessFully Submitted !!';
                                    yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 1, this.currentUser.id, this.request.body.qid));
                                } else
                                    response = 'Already Submitted !!';
                            }
                        }
                    }
                    else if (type[0].toLowerCase() === 'delete' || type[0].toLowerCase() === 'alter') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm' + this.currentUser.id);  // replace 1 with this.currentUser.id
                        response = 'SuccessFully Submitted !!';
                        var name = 'p' + this.currentUser.id;
                        var databasename = 'm' + this.currentUser.id;
                        var res2 = execSync('mysqldump -uroot -proot ' + databasename + '>static/static/uploads/' + name + '.sql');
                        var file1 = fs.readFileSync('static/static/uploads/' + name + '.sql', 'utf-8').toString().split('\n');
                        var file2 = fs.readFileSync('static/static/uploads/m' + this.request.body.qid + '.sql', 'utf-8').toString().split('\n');

                        console.log(file1);
                        console.log(file2);

                        var maxlen = file1.length;
                        if (file2.length > file1.length) maxlen = file2.length;

                        try {

                            for (var i = 3; i < maxlen - 2; ++i)
                                if (!(file2[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00') === file1[i].replace(/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g, '0000-00-00 00:00:00'))) {
                                    flag = 0;
                                    break;
                                }
                            yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, flag));

                        } catch (e) {
                            if (multipletype) {
                                yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', flag, this.currentUser.id, this.request.body.qid));
                            }
                        }

                        fs.unlinkSync('static/static/uploads/' + name + '.sql');
                    }
                    else if (type[0].toLowerCase() === 'submit') {
                        var qid = type[1].split('-')[1];
                        var user = this.currentUser;
                        sessionUtils.updateUserInSession(user, this.cookies);
                        user['qid'] = qid;
                        response = type[1] + ' Selected';
                    }
                    else {
                        response = 'Query Not Allowed';
                    }
                    // here give response to this.body
                    this.body = response;

                }
            catch (e)
                {
                    try{
                        yield databaseUtils.executeQuery(util.format('insert into submission(userid,qid,status) values("%s","%s","%s")', this.currentUser.id, this.request.body.qid, 0));
                    }catch (e) {
                        yield databaseUtils.executeQuery(util.format('update submission set status="%s" where userid="%s" and qid="%s"', 0, this.currentUser.id, this.request.body.qid));
                    }
                }
            }

                else if ( this.currentUser.pid!=undefined && type[0].toLowerCase() === 'submit' ){
                    if ( type[0].toLowerCase() === 'submit' ){
                        var qid = type[1].split('-')[1];
                        var user = this.currentUser;
                        sessionUtils.updateUserInSession(user, this.cookies);
                        user['qid'] = qid;
                        this.body = type[1] + ' Selected';
                    } else {
                        this.body='No Question Selected !!';
                    }
                }

                else if ( type[0].toLowerCase() === 'solve' && type.length>1){
                    try{
                        var pid = type[1].split('-')[1];
                        var user = this.currentUser;
                        user['pid'] = pid;
                        sessionUtils.updateUserInSession(user,this.cookies);
                        this.body = 'Practical ' + type[1]+' Selected !';
                    }catch (e) {
                        this.body = e.code;
                    }
                }

                else {
                    if (type[0].toLowerCase() === 'create' && type[1].toLowerCase() === 'table') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        this.body = 'Query OK, 0 row affected (0.01 sec)';
                    }
                    else if (type[0].toLowerCase() === 'insert' && type[1].toLowerCase() === 'into') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        this.body = 'Query OK, 1 row affected (0.01 sec)';
                    }
                    else if (type[0].toLowerCase() === 'update') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        this.body = 'Query OK, ' + res.affectedRows + ' rows affected (0.02 sec)\n' + res.message.toString().substring(1, res.message.length);
                    }
                    else if (type[0].toLowerCase() === 'drop' && type[1].toLowerCase() === 'table') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        this.body = 'Query OK, 0 row affected (0.01 sec)';
                    }
                    else if (type[0].toLowerCase() === 'show' && type[1].toLowerCase() === 'databases')
                        this.body = 'Error: You Do Not Have Access To Create, See And Drop Database';
                    else if (type[0].toLowerCase() === 'drop' && type[1].toLowerCase() === 'database')
                        this.body = 'Error: You Do Not Have Access To Create, See And Drop Database';
                    //   else if ( type[0].toLowerCase() === 'create' && type[1].toLowerCase() === 'database' )
                    //   this.body='Error: You Do Not Have Access To Create, See And Drop Database';
                    else if (type[0].toLowerCase() === 'show' && type[1].toLowerCase() === 'tables') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        if (res.length == 0) this.body = 'Empty set (0.00 sec)';
                        else {
                            var response = '+-----------------------------------------------------------------+' + '\n';
                            response += 'Tables' + '\n';
                            response += '+-----------------------------------------------------------------+' + '\n';
                            for (var i in res)
                                response += (res[i]['Tables_in_m' + this.currentUser.id] + '\n');  // replace 1 with this.currentUser.id
                            response += '+-----------------------------------------------------------------+';
                            this.body = response;
                        }
                    }
                    else if (type[0].toLowerCase() === 'select') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        console.log(res);
                        if (res.length == 0) this.body = 'Empty set (0.00 sec)';
                        else {
                            var space = {};
                            for (var i in res[0]) space[i] = i.toString().length;

                            for (var i in res) {
                                for (var j in res[i]) {
                                    var l;
                                    if (res[i][j]) l = res[i][j].toString().length;
                                    else l = 0;
                                    if (space[j] < l) space[j] = l + 1;
                                }
                            }

                            console.log(space);

                            response = '+';
                            for (var i in space) response += '-'.repeat(parseInt(space[i])) + '+';
                            response += '\n';

                            for (var i in space) response += i + ' '.repeat(parseInt(space[i]) - parseInt(i.length) + 1);
                            response += '\n';

                            response += '+';
                            for (var i in space) response += '+' + '-'.repeat(parseInt(space[i])) + '+';
                            response += '\n';


                            for (var i in res) {
                                for (var j in res[i]) {
                                    var l = 0;
                                    if (res[i][j]) l = res[i][j].toString().length;
                                    else length = space[j];
                                    response += res[i][j] + ' '.repeat(parseInt(space[j] - l + 1));
                                }
                                response += '\n';
                            }

                            console.log(response);

                            this.body = response;

                        }
                    }
                    else if (type[0].toLowerCase() === 'desc') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        console.log(res);

                        var space = {};
                        for (var i in res[0]) space[i] = i.toString().length;

                        for (var i in res) {
                            for (var j in res[i]) {
                                var l;
                                if (res[i][j]) l = res[i][j].length;
                                else l = 0;
                                if (space[j] < l) space[j] = l + 1;
                            }
                        }

                        console.log(space);

                        response = '+';
                        for (var i in space) response += '-'.repeat(parseInt(space[i]) + 1) + '+';
                        response += '\n';

                        for (var i in space) response += i + ' '.repeat(parseInt(space[i]) - parseInt(i.length) + 2);
                        response += '\n';
                        response += '+';

                        for (var i in space) response += '-'.repeat(parseInt(space[i]) + 1) + '+';
                        response += '\n';


                        for (var i in res) {
                            for (var j in res[i]) {
                                var l = 0;
                                if (res[i][j]) l = res[i][j].length;
                                else length = space[j];
                                response += res[i][j] + ' '.repeat(parseInt(space[j] - l + 1));
                            }
                            response += '\n';
                        }

                        console.log(response);

                        this.body = response;
                    }
                    else if (type[0].toLowerCase() === 'delete' || type[0].toLowerCase() === 'alter') {
                        var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);  // replace 1 with this.currentUser.id
                        this.body = 'Query OK, ' + res.affectedRows + ' rows affected (0.02 sec)\n' + res.message.toString().substring(1, res.message.length);
                    }
                    else if (type[0].toLowerCase() === 'submit') {
                        var qid = type[1].split('-')[1];
                        var user = this.currentUser;
                        sessionUtils.updateUserInSession(user, this.cookies);
                        user['qid'] = qid;
                        this.body = type[1] + ' Selected';
                    }
                    else {
                        // var res = yield mysqlUtils.executeQuery(util.format(myquery), 'm'+this.currentUser.id);
                        // this.body = res;
                        this.body='Query Not Allowed';
                    }
                }
            }

        }catch (e) {
            console.log(e);
            this.body=e.code;
        }
    },
    tempfunc:function *(next) {
        // var queries = fs.readFileSync('static/static/uploads/upload_774b74e2d15bdc57d3e986369791e822','utf-8');
        // console.log(queries);
        // var res = yield mysqlUtils.executeDump(util.format(queries),'db4');


        // var res = cmd.get('mysqldump -uroot -proot db4>mydb4.sql');
        // console.log(res);
        // console.log('done');
    },
}
