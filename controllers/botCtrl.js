var sessionUtils = require('../services/sessionUtils');
var quick = require('./quickstart');
var uuid = require('uuid');

module.exports = {
    showHomePage: function* (next) {
        var id;
        if (this.currentUser){
            id=this.cookies.get("SESSION_ID");
        } else {
            id='null';
        }
        yield this.render('bot',{
            id:id
        });
    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    },
    go:function *(next) {
        console.log('erhgseth hit api');
        var query = this.request.body.message.toString().toLowerCase();
        var sid = this.request.body.session;
        var x;
        console.log(query,sid);

        if (sid != 'null'){
            if (this.currentUser.id!=undefined){
                x = yield quick.executeCall(query,this.cookies.get("SESSION_ID")+'_'+this.currentUser.id);
            } else {
                x = yield quick.executeCall(query,this.cookies.get("SESSION_ID"));
            }


        } else {
            var user = {user:'active'};
            sid = uuid.v1();
            sessionUtils.saveUserInSession2(user,sid,this.cookies);
            x = yield quick.executeCall(query,sid);

        }
      //  x = yield quick.executeCall(query,'abcde-fghij-klmno-pqrst-uvwxy');

        this.body=x;
    }
}
