
var sessionUtils = require('./../services/sessionUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var redisUtils = require('./../services/redisUtils');
var util = require('util');
var fs = require('fs');
var GCM = require('node-crypto-gcm').GCM;
var gcm = new GCM('t86GvATWQV6S', {algorithm: 'aes-256-gcm',
    saltLenght: 123,
    pbkdf2Rounds: 1000,
    pbkdf2Digest: 'sha512'});
var nodemailer = require('nodemailer');

module.exports = {
    addcomment: function* (next) {
        console.log('api hit');
        console.log(this.request.body);
        var solution_id = this.request.body.solution_id;
        var line = this.request.body.line;
        var comment = this.request.body.comment;
        console.log(solution_id, line, comment);

        var res = yield databaseUtils.executeQuery(util.format('insert into comments (solution_id,line,comment) values("%s",%s,"%s")', solution_id, line, comment));

        var flag;
        if (res.length == 0) {
            flag = 0;
        }
        else
            flag = 1;
        this.body = {flag: flag};
    },
    getcomments: function* (next) {
        var res = yield databaseUtils.executeQuery(util.format('select * from comments where solution_id="%s"', this.request.body.solution_id));
        this.body = res;
    },
    getlabname: function* (next) {
        var lab_id = this.request.body.labcode;
        var res = yield databaseUtils.executeQuery(util.format('select * from lab where lab_id = "%s"', lab_id));

        if (res.length >= 1) {
            this.body = {labname: res[0].lab_name, flag: 1}
        } else {
            this.body = {flag: 0}
        }

    },
    enroll: function* (next) {
        var user_id = this.currentUser.user_id;
        var lab_id = this.request.body.labcode;
        var res = yield databaseUtils.executeQuery(util.format('insert into labrequest(user_id,lab_id) values("%s","%s")', user_id, lab_id));
        this.body = {};
    },
    getallRequests: function* (next) {
        var lab_id = this.request.body.lab_id;
        var res = yield databaseUtils.executeQuery(util.format('select user.* from labrequest,user where labrequest.user_id = user.user_id and labrequest.lab_id="%s"', lab_id));
        this.body = res;

    },
    grantrequest : function *(next) {
        var lab_id = this.request.body.lab_id;
        var user_id = this.request.body.user_id
        console.log(lab_id,user_id);
        var res = yield databaseUtils.executeQuery(util.format('insert into free_lab(user_id,lab_id) values("%s","%s")',user_id,lab_id));
        var res2 = yield databaseUtils.executeQuery(util.format('delete from labrequest where user_id="%s" and lab_id="%s"',user_id,lab_id));

        if (res.length>=1){
            this.body=1;
        }
        else{
            this.body=0;
        }

    },
    cancelrequest : function *(next) {
        var lab_id = this.request.body.lab_id;
        var user_id = this.request.body.user_id
        console.log(lab_id,user_id);
       // var res = yield databaseUtils.executeQuery(util.format('insert into free_lab(user_id,lab_id) values("%s","%s")',user_id,lab_id));
        var res2 = yield databaseUtils.executeQuery(util.format('delete from labrequest where user_id="%s" and lab_id="%s"',user_id,lab_id));


    },

    saveNonCodingContent:function *(next) {
        var solutionid = './Solution/'+this.request.body.solutionid;
        var data = this.request.body.data;


        var rid = this.currentUser.user_id;
        var problem_number = this.request.body.problem_number;
        var problem_id = this.request.body.problem_id;
        var pid = eval(problem_id);
        var pno = eval(problem_number);

        var queryString="insert into usr_prob_sub(user_id,problem_number,problem_id,submission_id) values ('%s','%s','%s','%s') ON DUPLICATE KEY UPDATE submission_id='%s'";
        var query=util.format(queryString,rid,pno,pid,1,1);

        var res = yield databaseUtils.executeQuery(query);

        fs.writeFile(solutionid,data,function (err) {
            if (err) console.log('error in writing file',err);
            else console.log('file written successfully');
        });



    },
    // tt:function *(next) {
    //     var name = this.request.body.name;
    //     var password = this.request.body.password;
    //    // this.cookies.set('mycookie','Hello');
    //     console.log(this.cookies.get('mycookie'));
    //     this.body={flag:"setted"};
    // }
}

