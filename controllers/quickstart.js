

'use strict';

// [START dialogflow_quickstart]

const dialogflow = require('dialogflow');
const uuid = require('uuid');

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */
async function runSample(query,sessionid) {
    // A unique identifier for the given session
    const sessionId = sessionid;
    const projectId = 'user-name-cafbb';
    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: query,
                // The language used by the client (en-US)
                languageCode: 'en-US',
            },
        },
    };

    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    console.log('Detected intent',responses);
    const result = responses[0].queryResult;
    //console.log(`  Query: ${result.queryText}`);
    //console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
        // console.log(`  Intent: ${result.intent.displayName}`);
        console.log(result.fulfillmentText);
        return result.fulfillmentText;
    } else {
        console.log(`No intent matched.`);
        return "I didn`t get What you just said !";
    }
}

module.exports = {
    executeCall : runSample
}
