var sessionUtils = require('./../services/sessionUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var redisUtils = require('./../services/redisUtils');
var mysqlUtils = require('./../services/mysqlUtils');
const { execSync  } = require('child_process');

var util = require('util');
var fs = require('fs');
var GCM = require('node-crypto-gcm').GCM;
var gcm = new GCM('t86GvATWQV6S', {algorithm: 'aes-256-gcm',
                                   saltLenght: 123,
                                   pbkdf2Rounds: 1000,
                                   pbkdf2Digest: 'sha512'});
var nodemailer = require('nodemailer');

module.exports = {

    showPStatus:function *(next) {
        var qid = this.params.qid;
        var lab = yield databaseUtils.executeQuery(util.format('select * from lab where id=(select labid from practical where id=(select pid from problem where id="%s"))',qid))

        var res = yield databaseUtils.executeQuery(util.format('select u.name,s.status from user u left join (select * from submission where qid="%s") s on u.id=s.userid where u.id in (select ul.userid from userlab ul where ul.labid=(select labid from practical where id=(select pid from problem where id="%s")))',qid,qid));

        var q = yield databaseUtils.executeQuery(util.format('select * from problem where id="%s"',qid));

        yield this.render('pstatus',{
            page:'lab',
            students:res,
            qname:q[0].problem,
            lab:lab[0]
        });

    },

    labstudents:function *(next) {
        var lid = this.params.lid;

        var l = yield databaseUtils.executeQuery(util.format('select * from lab where id="%s"',lid));

        var students = yield databaseUtils.executeQuery(util.format('select distinct(u.id),name from user u,lab l,userlab ul where ul.userid=u.id and ul.labid=l.id and l.uid="%s" and l.id="%s" and ul.status=1 order by u.id',this.currentUser.id,lid));

        var res = yield databaseUtils.executeQuery(util.format('select s.userid,f.name,count(*) as total,sum(status) as correct from submission s,(select u.* from user u left join userlab ul on u.id=ul.userid left join lab l on ul.labid=l.id where l.uid="%s" and l.id="%s") f where s.userid=f.id group by s.userid order by s.userid',this.currentUser.id,lid));

        if (this.currentUser.role==2 && l[0].uid==this.currentUser.id){

            yield this.render('labstudents',{
                page:'student',
                students:students,
                status:res,
                labname:l[0].labname

            });} else{
            this.redirect('/dashboard');
        }

    },

    studentStat:function *(next) {
        var sid = this.params.sid;
        var r1 = yield databaseUtils.executeQuery(util.format('select count(*) as total,sum(status) as correct from submission where userid="%s"',sid));
        var r2 = yield databaseUtils.executeQuery(util.format('select count(*) as labs from userlab where userid="%s"',sid));
        var r3 = yield databaseUtils.executeQuery(util.format('select count(*) as problems from userlab ul,practical p,problem q where ul.userid="%s" and p.labid=ul.labid and q.pid=p.id',sid));
        var r4 = yield databaseUtils.executeQuery(util.format('select count(*) as submission,date(ts) as date from submission where userid="%s" group by date(ts) limit 10',sid));
        var data = []; var axes = [];
        for(var i in r4){
            data.push(parseInt(r4[i].submission))
            axes.push( r4[i].date.toString().substring(4,10) );
        } console.log(data,axes);


        yield this.render('studentdash', {
            page:'student',
            total:r1[0].total,
            correct:r1[0].correct,
            labs:r2[0].labs,
            problems:r3[0].problems,
            data:data,
            axes:axes
        });
    },

    mydumps:function *(next) {
		var res = yield databaseUtils.executeQuery(util.format('select * from mydump where uid="%s"',this.currentUser.id));

		yield this.render('mydump',{
			page:'mydumps',
			dump:res
		});
    },

    dropdb:function *(next) {
		var res = yield databaseUtils.executeQuery(util.format('drop database m'+this.currentUser.id));
		console.log('here',res);
		this.body={};
    },

	hidep:function *(next) {
		var pid = this.request.body.pid;
		var res = yield databaseUtils.executeQuery(util.format('update practical set active=1 where id="%s"',pid));
		var lid = yield databaseUtils.executeQuery(util.format('select labid from practical where id="%s"',pid));
		this.redirect('/practicals/'+lid[0].labid);
    },
	deletep:function *(next) {
        var pid = this.request.body.pid;
        var res = yield databaseUtils.executeQuery(util.format('update practical set active=0 where id="%s"',pid));
        var lid = yield databaseUtils.executeQuery(util.format('select labid from practical where id="%s"',pid));
        this.redirect('/practicals/'+lid[0].labid);
    },
	showp:function *(next) {
        var pid = this.request.body.pid;
        var res = yield databaseUtils.executeQuery(util.format('update practical set active=2 where id="%s"',pid));
        var lid = yield databaseUtils.executeQuery(util.format('select labid from practical where id="%s"',pid));
        this.redirect('/practicals/'+lid[0].labid);
    },
    labaccept:function *(next) {
		var uid = this.request.body.uid;
        var lid = this.request.body.lid;
        console.log(uid,lid);

        var res = yield databaseUtils.executeQuery(util.format('insert into userlab (userid,labid,status) values("%s","%s",1)',uid,lid));
        res = yield databaseUtils.executeQuery(util.format('delete from labrequest where labid="%s" and userid="%s"',lid,uid));
        this.redirect('/labrequest');
    },
    labreject:function *(next) {
        var uid = this.request.body.uid;
        var lid = this.request.body.lid;
        var res = yield databaseUtils.executeQuery(util.format('delete from labrequest where labid="%s" and userid="%s"',lid,uid));
        this.redirect('/labrequest');
    },
    requestnewlab:function *(next) {
		var userid = this.currentUser.id;
		var labcode = this.request.body.labcode;

		console.log(userid,labcode);

		var res = yield databaseUtils.executeQuery(util.format('insert into labrequest (userid,labid) select "%s",id from lab where labcode="%s"',userid,labcode));
		this.status=200;
    },
    labrequest:function *(next) {
		var req = yield databaseUtils.executeQuery(util.format('select u.name as uname,u.id as uid,l.labname, l.id as lid from lab l, user u, labrequest ul where l.uid="%s" and l.id=ul.labid and u.id=ul.userid and ul.status=0',this.currentUser.id));
		yield this.render('labrequest',{
			page:'student',req:req
		})
    },
    addproblem2:function *(next) {
		var pid = this.request.body.pid;
		var problem = this.request.body.problem;
		var wholeQuery = this.request.body.query;
        var query = this.request.body.query.toString().replace(/\s\s+/g,' ').split(' ');
        var type;
        var rees;
        var dump;

        // preparing database for query
        rees = yield databaseUtils.executeQuery(util.format('select * from problem where pid="%s" and type=2 order by id desc limit 1',pid));

        console.log(rees,'database dump to be executed');

        if (rees.length==0) { rees = yield databaseUtils.executeQuery(util.format('select * from practical where id="%s"',pid));
            dump = rees[0].dump;
        } else{
            dump = rees[0].dump+'.sql';
		}

        console.log(rees,'database dump to be executed');

        yield databaseUtils.executeQuery(util.format('create database mtest'+this.currentUser.id));

        var queries = fs.readFileSync('static/static/uploads/'+dump,'utf-8');

        res = yield mysqlUtils.executeDump(util.format(queries),'mtest'+this.currentUser.id);

        console.log('dump executed upto here',res);

		// throw Error;

        if ( query[0].toLowerCase() === 'select' || query[0] === 'desc' || query[0] === 'show' ){
        	type=1;
            var res = yield databaseUtils.executeQuery(util.format('insert into problem (problem,pid,dump,type) values("%s","%s","%s","%s")',problem,pid,'abcde',1));
            var pno = res.insertId;
            res = yield mysqlUtils.executeQuery(util.format(wholeQuery),'mtest'+this.currentUser.id);
            fs.writeFile('static/static/uploads/m'+pno,JSON.stringify(res),function (err) {
				if (err) console.log('error while writing file');
				else console.log('File Written Successfully','m'+pno);
            });
            var res2 = yield databaseUtils.executeQuery(util.format('select count(*) as total from problem where pid="%s"',pid));
            yield databaseUtils.executeQuery(util.format('update problem set pno="%s",dump="%s" where id="%s"',res2[0].total,'m'+pno,pno));
		} else {
        	type=2;
            var res = yield databaseUtils.executeQuery(util.format('insert into problem (problem,pid,dump,type) values("%s","%s","%s","%s")',problem,pid,'abcde',2));
            var pno = res.insertId;
            res = yield mysqlUtils.executeQuery(util.format(wholeQuery),'mtest'+this.currentUser.id);

            // throw Error;

            var name = 'm'+pno;
            var databasename = 'mtest'+this.currentUser.id;
            var res = execSync('mysqldump -uroot -proot '+databasename+' > static/static/uploads/'+name+'.sql');
            console.log(res);

            var res2 = yield databaseUtils.executeQuery(util.format('select count(*) as total from problem where pid="%s"',pid));
            yield databaseUtils.executeQuery(util.format('update problem set pno="%s",dump="%s" where id="%s"',res2[0].total,name,pno));
		}

        yield databaseUtils.executeQuery(util.format('drop database mtest'+this.currentUser.id));
        console.log('Problem Added Successfully');

		this.redirect('/problems/'+pid);
    },
    showProblems:function *(next) {
		var pid = this.params.pid;
		var meta = yield databaseUtils.executeQuery(util.format('select p.name as pname,p.id as pid,l.id as lid,l.labname as labname from practical p,lab l where p.id="%s" and p.labid=l.id',pid));
		var problems = yield databaseUtils.executeQuery(util.format('select * from problem where pid="%s"',pid));

		var allproblems = yield databaseUtils.executeQuery(util.format('select p.*,s.status as st  from problem p left join (select * from submission where userid="%s") s on p.id=s.qid where p.pid="%s"',this.currentUser.id,pid));

		if (allproblems.length==0){
            var allproblems = yield databaseUtils.executeQuery(util.format('select p.*,"%s" as st  from problem p where p.pid="%s"',0,pid));
        }

		console.log(meta);
		console.log(allproblems);

		yield this.render('problems',{
			page:'practical',
            meta:meta[0],
			problems:problems,
            allproblems:allproblems,
		});
    },
    addnewpractical : function *(next) {
		console.log(this.request.body);

		var practical = this.request.body.fields.practical;
		var labid = this.request.body.fields.labid;
		var dump = this.request.body.files.dump.path.split("\\")[3];
		var multiple = this.request.body.fields.multiple;
		if (multiple==undefined) multiple=0;
		var res = yield databaseUtils.executeQuery(util.format('insert into practical(labid,name,dump,multiple) values("%s","%s","%s","%s")',labid,practical,dump,multiple));
		this.redirect('/labs');
    },
    showPractical : function *(next) {
		var labid = this.params.pid;

        var practical = yield databaseUtils.executeQuery(util.format('select * from practical where labid="%s"',labid));

        var lab = yield databaseUtils.executeQuery(util.format('select * from lab where id="%s"',labid));

        yield this.render('practical',{
        	page:'practical',
			practicals:practical,
			lab:lab[0]
		});
    },
    addnewLab: function *(next) {
		var id = this.currentUser.id;
		var labname = this.request.body.labname;
		var labcode = this.request.body.labcode;
		console.log('Adding New Lab',id,labname,labcode);

		var res = yield databaseUtils.executeQuery(util.format('insert into lab(uid,labname,labcode) values("%s","%s","%s")',id,labname,labcode));
		this.status=200;

    },
    showGrade:function *(next) {
		yield this.render('grade',{
			page:'grade',
		})
    },
	showStudents: function *(next) {

    	var students = yield databaseUtils.executeQuery(util.format('select distinct(u.id),name from user u,lab l,userlab ul where ul.userid=u.id and ul.labid=l.id and l.uid="%s" and ul.status=1 order by u.id',this.currentUser.id));

        var res = yield databaseUtils.executeQuery(util.format('select u.name,userid,count(userid) as total,sum(status) as correct from submission left join user u on submission.userid=u.id where qid in (select id from problem where pid in (select id from practical where labid in (select id from lab where uid="%s"))) group by userid order by userid',this.currentUser.id));

        if (this.currentUser.role==2){

		yield this.render('student',{
			page:'student',
            students:students,
            status:res

        });} else{
    		this.redirect('/dashboard');
		}
    },
    showProfile : function *(next) {

    	yield this.render('profile',{page:'profile'});
    },
    showLabs : function *(next){
    	var id = this.currentUser.id;
    	console.log(this.currentUser);
    	if (this.currentUser.role==2) {
            var labs = yield databaseUtils.executeQuery(util.format('select lab.* from user,lab where user.id=lab.uid and user.id="%s"', id));
        } else {
            var labs = yield databaseUtils.executeQuery(util.format('select l.* from userlab ul,lab l where ul.labid=l.id and ul.userid="%s" and ul.status=1', id));
		}
    	yield this.render('labs',{labs:labs,page:'labs'});
	},
    showDashboard: function* (next) {

        if (this.currentUser.role==2){

            var requests = yield databaseUtils.executeQuery(util.format('select count(*) as c from user u left join labrequest lr on u.id=lr.userid left join lab l on l.id=lr.labid where l.uid="%s"',this.currentUser.id));
            var labs = yield databaseUtils.executeQuery(util.format('select count(*) as c from lab where uid="%s"',this.currentUser.id));
            var students = yield databaseUtils.executeQuery(util.format('select count(distinct ul.userid) as c from lab l left join userlab ul on l.id=ul.labid where l.uid="%s"',this.currentUser.id));
            var mylabs = yield databaseUtils.executeQuery(util.format('select lab.* from user,lab where user.id=lab.uid and user.id="%s"', this.currentUser.id));
            var mystudents = yield databaseUtils.executeQuery(util.format('select distinct(u.id),name from user u,lab l,userlab ul where ul.userid=u.id and ul.labid=l.id and l.uid="%s" and ul.status=1 order by u.id',this.currentUser.id));

            yield this.render('dashboard', {
                page:'dashboard',
                requests:requests[0].c,
                labs:labs[0].c,
                students:students[0].c,
                mylabs:mylabs,
                mystudents:mystudents
            });
        } else  if (this.currentUser.role == 1) {

            var r1 = yield databaseUtils.executeQuery(util.format('select count(*) as total,sum(status) as correct from submission where userid="%s"',this.currentUser.id));
            var r2 = yield databaseUtils.executeQuery(util.format('select count(*) as labs from userlab where userid="%s"',this.currentUser.id));
            var r3 = yield databaseUtils.executeQuery(util.format('select count(*) as problems from userlab ul,practical p,problem q where ul.userid="%s" and p.labid=ul.labid and q.pid=p.id',this.currentUser.id));
            var r4 = yield databaseUtils.executeQuery(util.format('select count(*) as submission,date(ts) as date from submission where userid="%s" group by date(ts) limit 10',this.currentUser.id));

            var data = []; var axes = [];
            for(var i in r4){
                data.push(parseInt(r4[i].submission))
                axes.push( r4[i].date.toString().substring(4,10) );
            } console.log(data,axes);


            yield this.render('studentdash', {
                page:'dashboard',
                total:r1[0].total,
                correct:r1[0].correct,
                labs:r2[0].labs,
                problems:r3[0].problems,
                data:data,
                axes:axes
            });
        } else {
            this.redirect('/');
        }

    },

    showSignupPage: function* (next) {
	var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            this.redirect('/uinfo');
        }
        var errorMessage;
        yield this.render('signup', {
            errorMessage: errorMessage
        });
	},
	
    showforgotpass: function* (next) {
	var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            this.redirect('/uinfo');
        }
        var errorMessage;
        yield this.render('forgot_password');
	},
	
    forgotpass: function* (next) {
	var email= this.request.body.email;
	//console.log(email);
	var queryString = "select user_id from user where email_id= '%s'";
        var query = util.format(queryString,email);
	var results = yield databaseUtils.executeQuery(query);
	//console.log(results.length);
	if(results.length == 0) {
            errorMessage = "Invalid Email-id";
            yield this.render('home', {
                errorMessage: errorMessage
            });}

	else{
	    var uid=results[0].user_id;
	    //console.log(uid);

 	    var hash = gcm.encrypt(uid);
	    //console.log(hash);
	    var querytwo = "insert into updatepassword(user_id,hash_val) values ('%s','%s') ON DUPLICATE KEY UPDATE hash_val='%s'";
            var querythree = util.format(querytwo,uid,hash,hash);
	    var results = yield databaseUtils.executeQuery(querythree);
	    var transporter = nodemailer.createTransport({
  service: 'gmail',
  secure: false,
  port: 25,
  auth: {
    user: 'tymly360@gmail.com',
    pass: 'webappdeploy'
  },
  tls: {
    rejectUnauthorized: false
  }
});

var HelperOptions = {
  from: 'tymly360@gmail.com',
  to: email,
  subject: 'Digilab Account Recovery',
  text: 'Click the link to change password :- \n'+ config.applicationUrl+'/recovery/'+hash
};
  transporter.sendMail(HelperOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }

  });
  errorMessage = "Check registered Email";
            yield this.render('home', {
                errorMessage: errorMessage
            });
	}

    },

    changepass: function* (next){
	    var hash=this.params.hash;
	    var uid=gcm.decrypt(hash);
	    var queryStringone = "select * from updatepassword where user_id= '%s'";
            var queryone = util.format(queryStringone,uid);
	    var results = yield databaseUtils.executeQuery(queryone);
	    //console.log(results);
	    if(results.length == 0) {
            errorMessage = "some error occured";
            yield this.render('home', {
                errorMessage: errorMessage
            });}
	    else{
	    var queryString = "delete from updatepassword where user_id= '%s'";
            var query = util.format(queryString,uid);
	    var results = yield databaseUtils.executeQuery(query);

        yield this.render('change_password', {
            uid: uid
        });}
   },
    updatepass:  function* (next){
	var user_id = this.request.body.uid;
        var password = this.request.body.password;
	var queryString = "update user set password='%s' where user_id= '%s'";
        var query = util.format(queryString,password,user_id);
	var results = yield databaseUtils.executeQuery(query);
	errorMessage = "Password Updated";
            yield this.render('home', {
                errorMessage: errorMessage
            });

},

    showHomePage: function* (next){
	var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
		var obj= yield redisUtils.getItem(sessionId);
		if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}
		if(obj.user.role_name=="student")
		{
            this.redirect('/uinfo');}
		if(obj.user.role_name=="teacher")
		{
            this.redirect('/facultylab');}
	if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}

        }

	errorMessage = "";
        yield this.render('login');
    },

    login: function* (next) {

        var email = this.request.body.email;
        var password = this.request.body.password;

        if (email.length!=0 && password.length!=0){
        	var res = yield databaseUtils.executeQuery(util.format('select u.*,r.type as role from user u left join role r on u.id=r.userid where u.email="%s" and u.password="%s"',email,password));
        	if (res.length==0){
                yield this.render('login',{msg:'Invalid E-mail or Password !!!'});
			} else{
        		console.log(res[0]);
        		sessionUtils.saveUserInSession(res[0],this.cookies);
        		this.redirect('/dashboard');
			}
		} else {
        	yield this.render('login',{msg:'Credentials Cannot Be Empty !!!'});
		}


	},
admin: function* (next){
	var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
		var obj= yield redisUtils.getItem(sessionId);
		if(obj.user.role_name=="admin")
		var Message="";
		{yield this.render('admin',{Message:Message});
            //this.redirect('/admin');}
}
		if(obj.user.role_name=="student")
		{
            this.redirect('/uinfo');}
		if(obj.user.role_name=="teacher")
		{
            this.redirect('/facultylab');}

}}
    ,
    facultylab: function* (next){
	var usersid=this.cookies.get("SESSION_ID");
	var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
		var obj= yield redisUtils.getItem(sessionId);
		if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}
		if(obj.user.role_name=="student")
		{
            this.redirect('/uinfo');}

	if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}}

	var obj= yield redisUtils.getItem(usersid);
	var id=obj.user.user_id;
	var queryString="select lab.lab_name,lab.lab_id,faculty_lab.section,count(labrequest.ts) as requests from faculty_lab inner join lab left join labrequest on lab.lab_id = labrequest.lab_id where faculty_lab.user_id= '%s' and lab.lab_id=faculty_lab.lab_id group by faculty_lab.user_id,lab.lab_name,lab.lab_id,faculty_lab.section";
	var query=util.format(queryString,id);
	var results=yield databaseUtils.executeQuery(query);

	//var re={};
	//re[1]=results;

	yield this.render('facultylab',{obj:JSON.stringify(results),uid:id});


	},

signup: function* (next) {
	var email = this.request.body.email;
	var name = this.request.body.name;
	var password = this.request.body.password;
	var role = this.request.body.role;
	console.log(email,name,password,role);


	if (email.length!=0 && name.length!=0 && password.length!=0 && role.length!=0){
		if (role==0){
            yield this.render('signup',{msg:'User Type not Selected !!'});
		} else {
            try {
                var res = yield databaseUtils.executeQuery(util.format('insert into user(name,email,password) values("%s","%s","%s")', name, email, password));
                var user_id = res.insertId;
                res = yield databaseUtils.executeQuery(util.format('insert into role(userid,type) values("%s","%s")', user_id, role));
                yield this.render('login',{msg:'Successfully Signed Up !!\n Login To Continue...'});
            } catch (e) {
            	if (e.code === 'ER_DUP_ENTRY'){
            		yield this.render('signup',{msg:'User Already Exists !!'});
				} else{
            		console.log(e);
                    yield this.render('signup',{msg:'Error While Signup !!'});
				}
            }
        }
	} else{
        yield this.render('signup',{msg:'Fields Cannot Be Empty !!'});
	}

	},

    uinfo: function* (next){
	var sessionId = this.cookies.get("SESSION_ID");
	console.log(sessionId);
	 if(sessionId) {
		var obj= yield redisUtils.getItem(sessionId);
		if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}

		if(obj.user.role_name=="teacher")
		{
            this.redirect('/facultylab');}
	if(obj.user.role_name=="admin")
		{
            this.redirect('/admin');}

        }


	var usersid=this.cookies.get("SESSION_ID");
	var obj= yield redisUtils.getItem(usersid);
	var cid=obj.user.course_id;
	var llist = yield redisUtils.getItem("lablist");
	var id=obj.user.user_id;
	var year = obj.user.year;

	var queryString = "select lab.lab_name from lab inner join year_lab where year_lab.lab_id=lab.lab_id and year_lab.course_id = '%s' and year_lab.year='%s'";
        var query = util.format(queryString, cid,year);
	var results = yield databaseUtils.executeQuery(query);

	var results2 = yield databaseUtils.executeQuery(util.format('select l.* from free_lab fl left join lab l on fl.lab_id = l.lab_id where fl.user_id="%s"',id));

	var labinfo={labs:{}};
	for (var i in results)
	{
		var lab_id=results[i];
		var labname=i;
		labinfo.labs[labname]=lab_id;
	}




	yield this.render('uinfo',{llist,labinfo: {labinfo,id,llist},free_labs:results2});
	},

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        this.redirect('/');
    },
 ide:  function* (next) {
	var usersid=this.cookies.get("SESSION_ID");
	var obj= yield redisUtils.getItem(usersid);
	var rid=obj.user.user_id;
	var problem=this.request.body.problem;
  var problem_number = this.request.body.problem_number;
	var problem_id = this.request.body.problem_id;
	var dirname = rid + '_' + problem_number + '_' + problem_id;
  var path="./Solution/"+dirname;
  var data='';
  if(fs.existsSync(path)){
    data = fs.readFileSync(path,'utf8');
  }

	yield this.render('ideindex', {
              user_id: {rid,problem,problem_number,problem_id,data}
            });

	},
    opennoncodinglab:  function* (next) {
        var usersid=this.cookies.get("SESSION_ID");
        var obj= yield redisUtils.getItem(usersid);
        var rid=obj.user.user_id;
        var problem=this.request.body.problem;
        var problem_number = this.request.body.problem_number;
        var problem_id = this.request.body.problem_id;

        var labname = this.request.body.labname;

        var dirname = rid + '_' + problem_number + '_' + problem_id;
        var path="./Solution/"+dirname;
        var data='';
        if(fs.existsSync(path)){
            data = fs.readFileSync(path,'utf8');
        }

        console.log(rid,problem,problem_number,problem_id,data,labname);
        yield this.render('nc2', {
            user_id: {rid,problem,problem_number,problem_id,data},
            labname:labname,
        });

    },
review:  function* (next) {
	//var usersid=this.cookies.get("SESSION_ID");
	//var obj= yield redisUtils.getItem(usersid);
	//var rid=obj.user.user_id;


	var rid = this.currentUser.user_id;
	var problem_req= this.request.body.problem;
	var problem_number = this.request.body.problem_number;
	var problem_id = this.request.body.problem_id;
	var pid = eval(problem_id);
	var pno = eval(problem_number);

	console.log(rid,pid,pno);

	var queryString="insert into usr_prob_sub(user_id,problem_number,problem_id,submission_id) values ('%s','%s','%s','%s') ON DUPLICATE KEY UPDATE submission_id='%s'";
	var query=util.format(queryString,rid,pno,pid,1,1);

	var results=yield databaseUtils.executeQuery(query);

	var code =this.request.body.source_code;
	var filename=rid+'_'+pno+'_'+pid;




	fs.writeFile("./Solution/"+filename,code,function (err) {
		if (err) console.log('error in writing program');
		else console.log('Code successfully written in file');
    });

    console.log('Saving program',problem_number,problem_id);

	this.redirect("/uinfo");
	},

reviewcode: function* (next){
	var userid=this.request.body.userid;
	var problem_req= this.request.body.problem;
	var problem_number = this.request.body.problem_number;
	var problem_id = this.request.body.problem_id;
    var lab_type = this.request.body.lab_type;


    console.log("problem_number",problem_number,'lab type',lab_type);

        //var problem=problem_req.substring(0, 200);
	var dirname=userid+'_'+problem_number+'_'+problem_id;
	var fileName="./Solution/"+dirname;
	var code;



       {
           data =fs.readFileSync(fileName,'utf8');

        }
        if (lab_type==1)

 yield this.render('console',{solution_id:dirname,data:data, user_id:userid, problem:problem_req, problem_number:problem_number, problem_id:problem_id});
		else
			yield this.render('noncodingconsole',{solution_id:dirname,data:data, user_id:userid, problem:problem_req, problem_number:problem_number, problem_id:problem_id});

},

admintask: function* (next){
	var Message;
	var facultyid=this.request.body.facultyid;
	var lab=this.request.body.labid;
	var sec=this.request.body.section;
	var checkquery="select user_id from user where user_id = '%s'";
	var cquery1 = util.format(checkquery, facultyid);
        var checkr = yield databaseUtils.executeQuery(cquery1);
	if(checkr!=0){
	var queryString="insert into faculty_lab(user_id,lab_id,section) values ('%s','%s','%s')";
	var query=util.format(queryString,facultyid,lab,sec);
	var results=yield databaseUtils.executeQuery(query);
	var queryString2="update role set role_name='teacher' where user_id='%s'";
	var query2=util.format(queryString2,facultyid);
	var results2=yield databaseUtils.executeQuery(query2);
	if(results.length!=0&&results2.length!=0)
	{Message="Lab Assigned";}
	else
	Message="Some error occured";
	yield this.render("admin",{Message:Message});}
	else{
		Message="Faculty does not exist";
	yield this.render("admin",{Message:Message});
}
},
admintasklab: function* (next){
	var Message;
	var labid=this.request.body.labid;
	var courseid=this.request.body.c_id;
	var year=this.request.body.year;
	var labname=this.request.body.labname;
	var checkquery="select lab_id from lab where lab_id = '%s'";
	var cquery1 = util.format(checkquery, labid);
        var checkr = yield databaseUtils.executeQuery(cquery1);
	if(checkr==0){
	var queryString="insert into lab(lab_id,lab_name) values ('%s','%s')";
	var query=util.format(queryString,labid,labname);
	var results=yield databaseUtils.executeQuery(query);
	var queryString2="insert into course_lab(course_id,lab_id) values ('%s','%s')";
	var query2=util.format(queryString2,courseid,labid);
	var results2=yield databaseUtils.executeQuery(query2);
	var queryString3="insert into year_lab(course_id,lab_id,year) values ('%s','%s','%s')";
	var query3=util.format(queryString3,courseid,labid,year);
	var results3=yield databaseUtils.executeQuery(query3);
	if((results.length!=0)&&(results2.length!=0)&&(results3.length!=0))
	{Message="Lab Added";}
	else
	Message="Some error occured";
	yield this.render("admin",{Message:Message});}
else{
yield this.render("admin",{Message:"Lab already exist"});
}

},

	//edit_profile function
    editProfilePage: function*(next){
        var errorMessage;
        yield this.render('/edit_profile', {
            errorMessage: errorMessage
        });
    },
	editProfile:function *(next) {
		try {

            var id = this.currentUser.id;

            var name = this.request.body.name;
            var email = this.request.body.email;
            var password = this.request.body.password;

            var res = yield databaseUtils.executeQuery(util.format('update user set name="%s",email="%s",password="%s" where id="%s"',name,email,password,id));
        yield this.render('login',{
			msg:'Details Updated !!\nLogin To Continue..'
		});
		}catch (e) {
			yield this.render('profile',{
				page:'profile',
				msg:'Error While Updating Details'
			});
        }




        },
	showLandingPage:function *(next) {
		var sessionId = this.cookies.get("SESSION_ID");
		if(this.currentUser)
		{
			this.redirect('/dashboard');
		}

		yield this.render('index');
	},
	
	requestForm:function *(next){
		var name = this.request.body.name;
		var email = this.request.body.email;
		var phone = this.request.body.phone;
        var msg = this.request.body.msg;

        var queryString = "insert into request_demo(name,email,phone_no,msg) values('%s','%s','%s','%s');";
		var query = util.format(queryString,name,email,phone,msg);
		var res = yield databaseUtils.executeQuery(query);
		this.redirect("/");
	},

    showNonCodingData : function *(next) {
		// console.log(this.request.body.content);
        // console.log(this.request.body.solutionid);
		//
		//
        // fs.writeFile("./Solution/"+this.request.body.solutionid,this.request.body.content,function (err) {
        //     if (err) console.log('error occured while writing file',err);
        //     console.log('Written FIle');
        // });

        var rid = this.currentUser.user_id;
        var problem_req= this.request.body.problem;
        var problem_number = this.request.body.problem_number;
        var problem_id = this.request.body.problem_id;
        var pid = eval(problem_id);
        var pno = eval(problem_number);

        console.log('Saving program',problem_number,problem_id);

        // var problem=problem_req.substring(0, 200);

        var queryString="insert into usr_prob_sub(user_id,problem_number,problem_id,submission_id) values ('%s','%s','%s','%s') ON DUPLICATE KEY UPDATE submission_id='%s'";
        var query=util.format(queryString,rid,pno,pid,1,1);

        var results=yield databaseUtils.executeQuery(query);

        var code =this.request.body.content;
        var filename=rid+'_'+pno+'_'+pid;




        fs.writeFile("./Solution/"+filename,code);

        this.redirect("/uinfo");


    },

}
