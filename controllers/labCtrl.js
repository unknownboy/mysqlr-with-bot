var redisUtils = require('./../services/redisUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var util = require('util');
var fs = require('fs');
var rn = require('random-number');

module.exports = {
    viewproblem: function* (next) {
        var labname = this.params.lab_name;
	var usersid=this.cookies.get("SESSION_ID");
	var obj= yield redisUtils.getItem(usersid);
        var id=obj.user.user_id;



        var queryString = "select lab_id,lab_type from lab where lab_name= '%s'";
        var query = util.format(queryString,labname);
        var labid = yield databaseUtils.executeQuery(query);
	var lid=0;
	var lid= labid[0].lab_id;
	var lab_type = labid[0].lab_type;

	var l=eval(JSON.stringify(lid));
	var status={status:{}};
        var queryString = "select problem_number,problem_id,problem from lab_problem where lab_id= '%s' and problem_flag=1 order by problem_number ASC";
        var query = util.format(queryString,l);

        var problems = yield databaseUtils.executeQuery(query);

	for (var i in problems){
	var problem=eval(JSON.stringify(problems[i].problem));
	var problem_number=(problems[i].problem_number);
	var problem_id=(problems[i].problem_id);

	var queryStringtwo = "select submission_id from usr_prob_sub where user_id='%s' and problem_number='%s' and problem_id='%s'";
	var querytwo = util.format(queryStringtwo,id,problem_number,problem_id);
	var stat = yield databaseUtils.executeQuery(querytwo);
	if(stat.length==0)
		var subid='0';
	else
		var subid=JSON.stringify(stat[0].submission_id);
	status.status[i]={problem:problem,problem_number:problem_number,problem_id:problem_id,submissionid:subid};

	}

        yield this.render('problem',{labname:labname,status: status,lab_type:lab_type});
    },


    facultyprob: function*(next){
      var labname= this.params.lab_name;
    	var sec=this.params.section;
    	var queryString = "select lab_id from lab where lab_name= '%s'";
    	var query = util.format(queryString,labname);
            var labid = yield databaseUtils.executeQuery(query);
    	var lid=0;
    	var lid= labid[0];
    	var l=eval(JSON.stringify(lid.lab_id));


            var queryString = "select problem_id, problem_number, problem,problem_flag from lab_problem where lab_id = '%s' order by problem_number ASC";
            var query = util.format(queryString,l);
            var problem = yield databaseUtils.executeQuery(query);



    	var queryStringtwo = "select count(user.user_id) as count from user inner join user_lab on user.user_id=user_lab.user_id inner join lab on user_lab.lab_id='%s' and lab.lab_id='%s' where user.user_section='%s'";

            var query = util.format(queryStringtwo,l,l,sec);
            var tn = yield databaseUtils.executeQuery(query);
    	var pc={info:{}};
    	for(var i in problem )
    	{var queryString2="select count(distinct(p.user_id)) as count from usr_prob_sub p inner join (select user.user_id from user inner join user_lab on user.user_id=user_lab.user_id inner join lab on user_lab.lab_id='%s' and lab.lab_id='%s' where user.user_section='%s')s on p.user_id=s.user_id where p.problem_number='%s' and p.problem_id='%s' AND p.submission_id='2'";
    	var querytwo=util.format(queryString2,l,l,sec,problem[i].problem_number,problem[i].problem_id);
    	var pcount = yield databaseUtils.executeQuery(querytwo);
    	pc.info[i]=pcount[0].count;
    	}


    	yield this.render('facultyprob',{labname:labname,problem: problem,section:sec,tn:tn[0].count,pc:pc});
	}
,
     addproblem: function*(next)
	{
	var pid=this.params.pid;
        var meta = yield databaseUtils.executeQuery(util.format('select p.name as pname,p.id as pid,l.id as lid,l.labname as labname from practical p,lab l where p.id="%s" and p.labid=l.id',pid));

	yield this.render('addproblem',{pid:pid,page:'practical',meta:meta[0]});
	},
	
      addprobindb: function*(next)
	{
  //console.log(Object.keys(this.request.body.data).length);
  var l=Object.keys(this.request.body.data).length;
	var problem_old = this.request.body.data[1].value;
	var problem_number = this.request.body.data[0].value;
  var labname = this.request.body.data[l-2].value;
	var section = this.request.body.data[l-1].value;
  var problem_quote=problem_old.replace(/'/g,'`');
	var problem_n=problem_quote.replace(/"/g,'``');
	var problem_m=problem_n.replace("\\n","");
	var problem_o=problem_m.replace(/(\r\n|\n|\r)/gm," ");
	var problem_p=problem_o.replace(/\r?\n|\r/g, " ");
	var problem_q=problem_p.replace(/\\r\\n/g, " ");
	var problem=problem_p.trim();
  var flag = 0;

	var queryString = "select lab_id from lab where lab_name= '%s'";
	var query = util.format(queryString,labname);
  var labid = yield databaseUtils.executeQuery(query);
	var lid=0;
	var lid= labid[0];
	var l=eval(JSON.stringify(lid.lab_id));
	var queryString = "insert into lab_problem(lab_id,problem,problem_number,problem_flag) values ('%s','%s','%s','%s')";
	var query = util.format(queryString,l,problem,problem_number,flag);
  var labid = yield databaseUtils.executeQuery(query);
  var queryString= "SELECT LAST_INSERT_ID() as pid";
  var p=yield databaseUtils.executeQuery(queryString);
  //console.log(p[0].pid);
  fs.writeFile("./static/testcases/"+p[0].pid+".js","var data="+JSON.stringify(this.request.body.data),function (err) {
      if (err) console.log('error in writing testcases');
  });
	var redirectUrl = "/"+labname+"/sec/"+section;
  this.status=200;
	// this.redirect(redirectUrl);
        this.body={};
	},

  deleteproblem: function*(next)
 {
 var problem_number = this.request.body.problem_number;

 //var problem = this.request.body.problem;
 var labname = this.request.body.lab_name;
 var problem_id = this.request.body.problem_id;


 var querystringone = "select lab_id from lab where lab_name = '%s'";
 var queryone =util.format(querystringone,labname);
 var labid = yield databaseUtils.executeQuery(queryone);
 var lab_id = labid[0].lab_id;


 //var queryString = "delete from lab_problem where lab_id='%s' and problem_number='%s' and problem_id='%s'";
 //var query = util.format(queryString,labid,problem_number,problem_id);
 //var result=yield databaseUtils.executeQuery(query);

 var querystring = "update lab_problem set problem_flag=0 where problem_id='%s' and problem_number='%s' and lab_id='%s';"
 var query = util.format(querystring,problem_id,problem_number,lab_id);
 var result=yield databaseUtils.executeQuery(query);

 this.redirect('back');
},
studentslistaccordingtolab: function*(next)
{
  var lab_name = this.request.body.lab_name;
  var section = this.request.body.section;

  var querystringone = "select lab_id,lab_type from lab where lab_name='%s'";
  var queryone = util.format(querystringone,lab_name);
  var labid = yield databaseUtils.executeQuery(queryone);
  var lab_id = labid[0].lab_id;
  var lab_type = labid[0].lab_type;

  var  querystringtwo = "select user.user_id,user.user_name from user inner join user_lab on user.user_id=user_lab.user_id where user.user_section = '%s' and user_lab.lab_id='%s'";
  var querytwo = util.format(querystringtwo,section,lab_id);
  var studentslist = yield databaseUtils.executeQuery(querytwo);

  var studentslist2 = yield databaseUtils.executeQuery(util.format('select user.user_id,user.user_name from user left join free_lab fl on user.user_id=fl.user_id where fl.lab_id="%s"',lab_id));


  yield this.render('studentslistaccordingtolab' , {studentslist:studentslist,lab_type:lab_type, lab_name:lab_name,lab_id:lab_id,section:section,studentslist2:studentslist2});

},

problemlistofstudent: function*(next)
{
  var lab_id = this.request.body.lab_id;
  var section = this.request.body.section;
  var user_id = this.request.body.user_id;
  var lab_type = this.request.body.lab_type;

  var status = {status:{}};

  var querystringzero = "select user_name from user where user_id = '%s'";
  var queryzero  = util.format(querystringzero, user_id);
  var username = yield databaseUtils.executeQuery(queryzero);

  var user_name = username[0].user_name;

  var querystringone = "select problem,problem_number,problem_id from lab_problem where lab_id = '%s'";
  var queryone = util.format(querystringone,lab_id);
  var results = yield databaseUtils.executeQuery(queryone);

  for(var problem in results)
  {
    var  problem_number = results[problem].problem_number;
    var problem_id = results[problem].problem_id;
    var querystringtwo = "select problem_number, problem_id,submission_id from usr_prob_sub where user_id = '%s' and problem_number='%s' and problem_id = '%s' ";
    var querytwo = util.format(querystringtwo,user_id,problem_number,problem_id);
    var details = yield databaseUtils.executeQuery(querytwo);

    if(details.length!= 0)
    {
    var  submission_id = details[0].submission_id
    }
    else {
    var  submission_id = 0;
    }

    status.status[problem] = {problem_id:problem_id, problem_number:problem_number, problem:eval(JSON.stringify(results[problem].problem)), submission_id:submission_id, lab_id:lab_id};

  }
console.log(status,lab_type,user_id,user_name);
  yield this.render('problemlistofstudent', {info:status,lab_type:lab_type,user_id:user_id,user_name:user_name});

},

restoreproblem: function*(next)
{
 var problem_number = this.request.body.problem_number;
 var problem_id = this.request.body.problem_id;
 var labname = this.request.body.lab_name;

 var querystringone = "select lab_id from lab where lab_name = '%s'";
 var queryone = util.format(querystringone,labname);
 var labid = yield databaseUtils.executeQuery(queryone);
 lab_id = labid[0].lab_id;



 var querystringtwo = "update lab_problem set problem_flag=1 where problem_id = '%s' and problem_number='%s' and lab_id='%s';"
 var querytwo = util.format(querystringtwo,problem_id,problem_number,lab_id);
 var results = yield databaseUtils.executeQuery(querytwo);

 this.redirect('back');
},

deleteproblempermanently: function*(next)
{
 var problem_number = this.request.body.problem_number;
 var problem_id = this.request.body.problem_id;
 var labname = this.request.body.lab_name;

 var querystringone = "select lab_id from lab where lab_name = '%s'";
 var queryone = util.format(querystringone,labname);
 var labid = yield databaseUtils.executeQuery(queryone);
 var lab_id = labid[0].lab_id;


 var querystringtwo = "delete from lab_problem where lab_id='%s' and problem_id='%s' and problem_number='%s';"
 var querytwo = util.format(querystringtwo, lab_id,problem_id,problem_number);
 var results = yield databaseUtils.executeQuery(querytwo);

 this.redirect('back');
},

rejectcode: function*(next)
{

    console.log('rejected');
 //var problem = this.request.body.problem;
 var source_code = this.request.body.source_code;
 var userid = this.request.body.userId;
 var problemnumber = this.request.body.problemNumber;
 var problemid = this.request.body.problemId;
 
 var user_id = eval(userid);
 var problem_number = eval(problemnumber);
 var problem_id = eval(problemid);
 
 //console.log(source_code, user_id, problem_number, problem_id);
 var filename=user_id+'_'+problem_number+'_'+problem_id;
 fs.writeFile("./Solution/"+filename,source_code,function (err) {
     if (err) console.log('error');
 });

 var querystringone = "Update usr_prob_sub set submission_id='3' where user_id='%s' and problem_number='%s' and problem_id='%s'";
 var queryone = util.format(querystringone,user_id,problem_number,problem_id);
 var results = yield databaseUtils.executeQuery(queryone);
 this.status=200;

},

  probstudent: function*(next){
	var problem=this.request.body.problem;
	var section=this.request.body.section;
	var labname=this.request.body.labname;
	var problem_number= this.request.body.problem_number;
	var problem_id = this.request.body.problem_id;


	var queryString = "select lab_id,lab_type from lab where lab_name= '%s'";
	var query = util.format(queryString,labname);
        var labid = yield databaseUtils.executeQuery(query);

	var lid=0;
	var lid= labid[0];
	var l=eval(JSON.stringify(lid.lab_id));
	var lab_type = eval(JSON.stringify(lid.lab_type));
	var status={status:{}};
	var queryString = "select user.user_name ,user.user_id from user inner join user_lab on user.user_id=user_lab.user_id where user.user_section='%s' and user_lab.lab_id='%s'";
	var query = util.format(queryString,section,l);
	var results = yield databaseUtils.executeQuery(query);

        console.log(results.length);

      var results2 = yield databaseUtils.executeQuery(util.format('select user.user_id,user.user_name from user left join free_lab fl on user.user_id=fl.user_id where fl.lab_id="%s"',l));

      console.log(results2.length);
      for(i in results2){
          results.push(results2[i]);
      }
      console.log(results.length);


      for(var i in results)
	{var userid=eval(JSON.stringify(results[i].user_id));
	var queryStringtwo = "select submission_id from usr_prob_sub where user_id='%s' and problem_number='%s' and problem_id='%s'";
	var querytwo = util.format(queryStringtwo,userid,problem_number,problem_id);
        var stat = yield databaseUtils.executeQuery(querytwo);
	if(stat.length!=0)
		var subid=JSON.stringify(stat[0].submission_id);
	else
		 var subid='0';

	status.status[i]={username:eval(JSON.stringify(results[i].user_name)),submissionid:subid,userid:userid};
	}




	yield this.render('studentstat',{results:status,problem:problem,lab_type:lab_type,labname:labname,section:section,problem_number:problem_number,problem_id:problem_id});


	},
approve: function*(next)
	{


	var userid=this.request.body.userId;
	var problemnumber =  this.request.body.problemNumber;
	var problemid = this.request.body.problemId;
	
	
	var user_id = eval(userid);
	var problem_number = eval(problemnumber);
	var problem_id = eval(problemid);

	console.log(userid,problemnumber,problemid);

	var queryString = "Update usr_prob_sub set submission_id='2' where user_id='%s' and problem_number='%s' and problem_id='%s'";
	var query = util.format(queryString,user_id,problem_number,problem_id);
	var results = yield databaseUtils.executeQuery(query);

	console.log('approving');
	this.status=200;
	this.body={};
	},

    addLab:function *(next) {

        var gen = rn.generator({
            min:  100000
            , max:  999999
            , integer: true
        });


        yield this.render('addlabpage',{
            labcode:gen(),
            err:undefined
        });
    },
    addLabProcedure: function *(next) {
        var id = this.currentUser.user_id;
        if (id){
            var labname = this.request.body.labname;
            var labcode = this.request.body.labcode;
            var labtype = this.request.body.labtype;
            var res = yield databaseUtils.executeQuery(util.format('insert into lab(lab_id,lab_name,lab_type) values("%s","%s","%s")',labcode,labname,labtype));
            res = yield databaseUtils.executeQuery(util.format('insert into faculty_lab(user_id,lab_id) values("%s","%s")',id,labcode));
        this.redirect('facultylab');
        }
        else{
            var err = 'Error...!!!\n Cannot Create Lab';
            var gen = rn.generator({
                min:  100000
                , max:  999999
                , integer: true
            });

            yield this.render('addlabpage',{
                labcode:gen(),
                err:err
            });
        }
    },
    showLabRequests : function *(next) {
        var user_id = this.currentUser.user_id;
        var lab_id = this.query.lab_id;
        var res = yield  databaseUtils.executeQuery(util.format('select user.*,lr.lab_id from faculty_lab fl left join labrequest lr on fl.lab_id = lr.lab_id left join user on lr.user_id = user.user_id where fl.user_id = "%s" and lr.lab_id = "%s"',user_id,lab_id));
        console.log(res);
        yield this.render('labrequest',{
            request : res
        });

    }
}
