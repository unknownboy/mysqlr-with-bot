-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gla_new
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(200) DEFAULT NULL,
  `solution_id` varchar(50) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (3,'2018-12-31 10:14:19','Costly Operation','4_1_107',1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_lab`
--

DROP TABLE IF EXISTS `course_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course_lab` (
  `course_id` int(11) DEFAULT NULL,
  `lab_id` varchar(25) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `course_id` (`course_id`),
  KEY `lab_id` (`lab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_lab`
--

LOCK TABLES `course_lab` WRITE;
/*!40000 ALTER TABLE `course_lab` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty_lab`
--

DROP TABLE IF EXISTS `faculty_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `faculty_lab` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `lab_id` varchar(25) DEFAULT NULL,
  `section` varchar(25) DEFAULT NULL,
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty_lab`
--

LOCK TABLES `faculty_lab` WRITE;
/*!40000 ALTER TABLE `faculty_lab` DISABLE KEYS */;
INSERT INTO `faculty_lab` VALUES (1,'102271',NULL,'2018-12-30 17:25:56'),(2,'978544',NULL,'2018-12-30 17:29:10'),(1,'476174',NULL,'2019-01-02 08:24:03'),(1,'194694',NULL,'2019-01-02 08:25:44'),(2,'540523',NULL,'2019-01-02 08:27:46');
/*!40000 ALTER TABLE `faculty_lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `free_lab`
--

DROP TABLE IF EXISTS `free_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `free_lab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned NOT NULL,
  `lab_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `c1` (`user_id`,`lab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `free_lab`
--

LOCK TABLES `free_lab` WRITE;
/*!40000 ALTER TABLE `free_lab` DISABLE KEYS */;
INSERT INTO `free_lab` VALUES (3,'2018-12-30 17:44:16',3,102271),(4,'2018-12-30 17:44:17',4,102271),(5,'2018-12-30 17:44:50',3,978544),(6,'2018-12-30 17:44:53',4,978544),(7,'2019-01-02 08:30:50',3,540523),(8,'2019-01-02 08:30:51',4,540523),(9,'2019-01-02 08:31:19',3,194694),(10,'2019-01-02 08:31:20',4,194694);
/*!40000 ALTER TABLE `free_lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab`
--

DROP TABLE IF EXISTS `lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lab` (
  `lab_id` varchar(25) NOT NULL,
  `lab_name` varchar(75) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lab_type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab`
--

LOCK TABLES `lab` WRITE;
/*!40000 ALTER TABLE `lab` DISABLE KEYS */;
INSERT INTO `lab` VALUES ('102271','Physics','2018-12-30 17:25:56',0),('194694','C Programming','2019-01-02 08:25:42',1),('476174','Java','2019-01-02 08:24:03',1),('540523','Chemistry','2019-01-02 08:27:46',0),('978544','Python','2018-12-30 17:29:10',1);
/*!40000 ALTER TABLE `lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_problem`
--

DROP TABLE IF EXISTS `lab_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lab_problem` (
  `lab_id` varchar(25) DEFAULT NULL,
  `problem` varchar(3072) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `problem_number` int(11) DEFAULT NULL,
  `problem_id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`problem_id`),
  KEY `lab_id` (`lab_id`),
  KEY `problem_id` (`problem`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_problem`
--

LOCK TABLES `lab_problem` WRITE;
/*!40000 ALTER TABLE `lab_problem` DISABLE KEYS */;
INSERT INTO `lab_problem` VALUES ('978544','Write a program to calculate the sum of two space separated integers numbers.','2018-12-30 17:30:17',1,107,1),('102271','Write Chemical Formula Of Water in correct Format.','2018-12-30 17:31:28',1,108,1),('194694','WAP to add two numbers','2019-01-02 08:36:13',1,109,1),('540523','Chemical formula of water.','2019-01-02 08:28:11',1,110,0);
/*!40000 ALTER TABLE `lab_problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labrequest`
--

DROP TABLE IF EXISTS `labrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `labrequest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned NOT NULL,
  `lab_id` varchar(50) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labrequest`
--

LOCK TABLES `labrequest` WRITE;
/*!40000 ALTER TABLE `labrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `labrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `problems` (
  `problem_id` int(11) NOT NULL,
  `problem_statement` varchar(300) DEFAULT NULL,
  `p_max_marks` int(11) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problems`
--

LOCK TABLES `problems` WRITE;
/*!40000 ALTER TABLE `problems` DISABLE KEYS */;
/*!40000 ALTER TABLE `problems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_demo`
--

DROP TABLE IF EXISTS `request_demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request_demo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL,
  `phone_no` varchar(10) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msg` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_demo`
--

LOCK TABLES `request_demo` WRITE;
/*!40000 ALTER TABLE `request_demo` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `role_name` varchar(75) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('teacher',1),('teacher',2),('student',3),('student',4);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submissions`
--

DROP TABLE IF EXISTS `submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `submissions` (
  `submission_id` int(11) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submissions`
--

LOCK TABLES `submissions` WRITE;
/*!40000 ALTER TABLE `submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updatepassword`
--

DROP TABLE IF EXISTS `updatepassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `updatepassword` (
  `user_id` int(10) unsigned NOT NULL,
  `hash_val` varchar(2048) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updatepassword`
--

LOCK TABLES `updatepassword` WRITE;
/*!40000 ALTER TABLE `updatepassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `updatepassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(75) NOT NULL,
  `password` varchar(75) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_id` int(11) DEFAULT NULL,
  `course_name` varchar(75) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `user_section` varchar(25) DEFAULT NULL,
  `year` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Tarun Garg','abcdefgh','2018-12-30 17:26:41',NULL,NULL,'tarun.garg_cs16@gla.ac.in',NULL,NULL),(2,'Shivanshu Tiwari','shivanshu@123','2018-12-30 17:28:11',NULL,NULL,'shivanshu.tiwari_cs16@gla.ac.in',NULL,NULL),(3,'Ankit Agrawal','ankit@123','2018-12-30 17:32:01',NULL,NULL,'ankit.agrawal_cs16@gla.ac.in',NULL,NULL),(4,'Gaurang Sharma','gaurang@123','2018-12-30 17:32:45',NULL,NULL,'gaurang.sharma_cs16@gla.ac.in',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_lab`
--

DROP TABLE IF EXISTS `user_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_lab` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `lab_id` varchar(25) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `lab_id` (`lab_id`),
  KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_lab`
--

LOCK TABLES `user_lab` WRITE;
/*!40000 ALTER TABLE `user_lab` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr_prob_sub`
--

DROP TABLE IF EXISTS `usr_prob_sub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usr_prob_sub` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `submission_id` int(11) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `problem_number` int(11) DEFAULT NULL,
  `problem_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr_prob_sub`
--

LOCK TABLES `usr_prob_sub` WRITE;
/*!40000 ALTER TABLE `usr_prob_sub` DISABLE KEYS */;
INSERT INTO `usr_prob_sub` VALUES (3,2,'2018-12-30 18:36:03',1,107),(3,2,'2018-12-30 18:10:24',1,108),(3,2,'2018-12-30 18:10:24',1,108),(3,2,'2018-12-30 18:36:03',1,107),(3,2,'2018-12-30 18:36:03',1,107),(3,2,'2018-12-30 18:36:03',1,107),(3,2,'2018-12-30 18:36:03',1,107),(4,3,'2018-12-30 18:10:49',1,108),(4,3,'2018-12-30 18:37:44',1,107),(4,1,'2018-12-31 10:49:07',1,107);
/*!40000 ALTER TABLE `usr_prob_sub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `year_lab`
--

DROP TABLE IF EXISTS `year_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `year_lab` (
  `year` varchar(25) DEFAULT NULL,
  `lab_id` varchar(25) DEFAULT NULL,
  `course_id` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `year_lab`
--

LOCK TABLES `year_lab` WRITE;
/*!40000 ALTER TABLE `year_lab` DISABLE KEYS */;
/*!40000 ALTER TABLE `year_lab` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 14:32:02
