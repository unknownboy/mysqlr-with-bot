-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: mysqlr
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lab`
--

DROP TABLE IF EXISTS `lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `labname` varchar(100) DEFAULT NULL,
  `uid` int(10) unsigned DEFAULT NULL,
  `labcode` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab`
--

LOCK TABLES `lab` WRITE;
/*!40000 ALTER TABLE `lab` DISABLE KEYS */;
INSERT INTO `lab` VALUES (1,'2019-01-17 07:21:12','DBMS Section A',3,83508915),(2,'2019-01-20 13:55:54','DBMS SEC-G',3,13189032),(3,'2019-01-21 07:49:23','DBMS Sec F',3,53172070),(4,'2019-01-21 08:03:37','DBMS LAB SEC D',3,62422214);
/*!40000 ALTER TABLE `lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labrequest`
--

DROP TABLE IF EXISTS `labrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `labrequest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `labid` int(10) unsigned DEFAULT NULL,
  `userid` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labrequest`
--

LOCK TABLES `labrequest` WRITE;
/*!40000 ALTER TABLE `labrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `labrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mydump`
--

DROP TABLE IF EXISTS `mydump`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mydump` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `dump` varchar(100) NOT NULL,
  `uid` int(10) unsigned DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mydump`
--

LOCK TABLES `mydump` WRITE;
/*!40000 ALTER TABLE `mydump` DISABLE KEYS */;
INSERT INTO `mydump` VALUES (1,'2019-01-18 21:17:11','m1_Sat_Jan_19_2019_02-47-11',1,''),(2,'2019-01-18 21:17:19','m1_Sat_Jan_19_2019_02-47-18',1,'ThisTime'),(3,'2019-01-19 05:24:13','m1_Sat_Jan_19_2019_10-54-12',1,''),(4,'2019-01-19 05:24:23','m1_Sat_Jan_19_2019_10-54-22',1,'gaurangdump'),(5,'2019-01-19 18:14:50','m1_Sat_Jan_19_2019_23-44-50',1,'P-1'),(6,'2019-01-19 20:12:40','m4_Sun_Jan_20_2019_01-42-40',4,'p1'),(7,'2019-01-21 07:59:43','m1_Mon_Jan_21_2019_13-29-42',1,'firstdone'),(8,'2019-01-21 08:12:51','m1_Mon_Jan_21_2019_13-42-48',1,'testpractical');
/*!40000 ALTER TABLE `mydump` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practical`
--

DROP TABLE IF EXISTS `practical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `practical` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `labid` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `dump` varchar(100) NOT NULL,
  `multiple` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practical`
--

LOCK TABLES `practical` WRITE;
/*!40000 ALTER TABLE `practical` DISABLE KEYS */;
INSERT INTO `practical` VALUES (1,'2019-01-17 13:35:07',1,'Insert Queries',2,'upload_774b74e2d15bdc57d3e986369791e822',1),(3,'2019-01-17 20:38:40',1,'My DB4',2,'upload_774b74e2d15bdc57d3e986369791e822',1),(4,'2019-01-20 13:58:50',2,'Mid First',2,'upload_385f2bfebe084d9bcc1319ec4c4f8c01',1),(5,'2019-01-21 07:51:42',3,'Show P',2,'upload_e55de35b7813a0a2e965558e1d7aeaac',1),(6,'2019-01-21 08:05:33',4,'Test Practical',2,'upload_7c3c191b52b938b1dab5d3df94b00601',1),(7,'2019-01-24 19:26:32',4,'Practical 5',1,'upload_38bdc0c5f2f375ca4272028478d97eff',0);
/*!40000 ALTER TABLE `practical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problem`
--

DROP TABLE IF EXISTS `problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `problem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pid` int(10) unsigned DEFAULT NULL,
  `problem` varchar(3072) DEFAULT NULL,
  `pno` int(10) unsigned DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `dump` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problem`
--

LOCK TABLES `problem` WRITE;
/*!40000 ALTER TABLE `problem` DISABLE KEYS */;
INSERT INTO `problem` VALUES (18,'2019-01-18 19:55:51',3,'create table myt2( ts timestamp default now(),name varchar(200));',1,2,'m3_Sat_Jan_19_2019_01-25-51.sql'),(19,'2019-01-18 19:56:45',3,'show contents of myt2',2,1,'m19.sql'),(20,'2019-01-18 20:02:02',3,'insert into myt2(name) values(\'ankit\');\r\n',3,2,'m3_Sat_Jan_19_2019_01-32-02.sql'),(22,'2019-01-18 20:04:19',3,'select * from myt2;',4,1,'m22.sql'),(23,'2019-01-19 20:03:39',3,'show all tables',5,1,'m23'),(24,'2019-01-20 13:59:38',4,'insert into user(name) values(\'ram\');',1,2,'m24'),(26,'2019-01-20 14:09:47',4,'alter table user add column status int default 0;',2,2,'m26'),(27,'2019-01-20 14:10:41',4,'update user set status=1;',3,2,'m27'),(28,'2019-01-20 14:11:36',4,'show tables;',4,1,'m28'),(29,'2019-01-20 14:11:48',4,'desc user;',5,1,'m29'),(31,'2019-01-21 07:55:24',5,'create table table123(name varchar(100));',1,2,'m31'),(32,'2019-01-21 07:57:24',5,'show tables;',2,1,'m32'),(33,'2019-01-21 08:06:59',6,'create table table123(name varchar(50));',1,2,'m33'),(34,'2019-01-21 08:07:20',6,'show tables;',2,1,'m34');
/*!40000 ALTER TABLE `problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` int(10) unsigned DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2019-01-15 19:29:09',1,1),(2,'2019-01-15 19:29:28',2,1),(3,'2019-01-15 20:16:29',3,2),(4,'2019-01-19 18:58:53',4,1),(5,'2019-01-22 21:00:09',5,1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `submission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `qid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid` (`userid`,`qid`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
INSERT INTO `submission` VALUES (17,'2019-01-19 12:42:04',1,1,18),(18,'2019-01-19 12:42:36',1,1,19),(19,'2019-01-19 12:42:59',1,1,20),(26,'2019-01-19 13:05:55',1,1,22),(27,'2019-01-19 19:40:33',4,1,18),(35,'2019-01-19 19:57:07',4,1,19),(36,'2019-01-19 19:57:32',4,1,20),(43,'2019-01-19 20:09:22',4,1,22),(45,'2019-01-19 20:21:17',1,1,23),(47,'2019-01-20 14:45:38',1,1,24),(101,'2019-01-21 07:58:48',1,1,31),(112,'2019-01-24 18:54:54',1,1,26),(114,'2019-01-24 19:02:26',1,1,27),(115,'2019-01-24 19:02:45',1,1,28),(116,'2019-01-24 19:03:00',1,1,29),(119,'2019-01-24 19:51:16',1,1,33);
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2019-01-15 19:29:08','Gaurang Sharma','gaurang@gmail.com','gaurang@123'),(3,'2019-01-15 20:16:28','Ankit Agrawal','ankit@gmail.com','ankit@1234'),(4,'2019-01-19 18:58:53','Tanishi','tanishi@gmail.com','tanishi@123'),(5,'2019-01-22 21:00:09','gigigigi','hello@gmail.com','ucyxydjcy');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userlab`
--

DROP TABLE IF EXISTS `userlab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `userlab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `labid` int(10) unsigned DEFAULT NULL,
  `userid` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userlab`
--

LOCK TABLES `userlab` WRITE;
/*!40000 ALTER TABLE `userlab` DISABLE KEYS */;
INSERT INTO `userlab` VALUES (1,'2019-01-17 18:38:34',1,1,1),(2,'2019-01-19 19:00:56',1,4,1),(3,'2019-01-20 14:21:20',2,1,1),(4,'2019-01-21 07:50:41',3,1,1),(5,'2019-01-21 08:04:18',4,1,1);
/*!40000 ALTER TABLE `userlab` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-25 16:30:28
