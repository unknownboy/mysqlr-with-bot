var toreplace = {
    '%' : '%25' ,
    ' ' : '%20' ,
    '!' : '%21' ,
    '"' : '%22' ,
    '#' : '%23' ,
    '$' : '%24' ,
    '&' : '%26' ,
    "'" : '%27' ,
    '(' : '%28' ,
    ')' : '%29' ,
    '*' : '%2A' ,
    '+' : '%2B' ,
    '.' : "%2E" ,
    '-' : '%2D' ,
    '/' : '%2F' ,
    ':' : '%3A' ,
    ';' : '%3B' ,
    '<' : '%3C' ,
    '=' : '%3D' ,
    '>' : '%3E' ,
    '?' : '%3F' ,
    '@' : '%40' ,
    '[' : '%5B' ,
    ']' : '%5D' ,
    '^' : '%5E' ,
    '_' : '%5F' ,
    '`' : '%60' ,
    '{' : '%7B' ,
    '|' : '%7C' ,
    '}' : '%7D' ,
    '~' : '%7E' ,
    '\n' : '%0A',
}
$(document).ready(function () {
   $('#myvisual').click(function () {
       var selected_language=parseInt( $('#selectLanguageBtn').val());
       if (selected_language>=17 && selected_language<=20) {
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }
       else if(selected_language>=3 && selected_language<=6){
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=c&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }
       else if(selected_language>=7 && selected_language<=10){
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=cpp&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }
       else if(selected_language>=13 && selected_language<=14){
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=java&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }
       else if(selected_language>=21 && selected_language<=24){
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=ruby&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }
       else if(selected_language>=25 && selected_language<=26){
           $('#pressModal').click();
           var text = sourceEditor.getValue();
           for (i in toreplace) {
               if (i == '(' || i == ')' || i == '[' || i == ']' || i == '{' || i == '}' || i == '*' || i == '+' || i == '?' || i == '|' || i == '.')
                   text = text.replace(new RegExp('\\' + i, "g"), toreplace[i]);
               else
                   text = text.replace(new RegExp(i, "g"), toreplace[i]);
               // console.log(i,toreplace[i],text);
           }
           console.log(text.substring(3, text.length - 3));
           var frame = "<iframe width=\"800\" height=\"500\" frameborder=\"0\" src=\"http://localhost:8003/iframe-embed.html#code=";
           frame = frame + text.substring(3, text.length - 3);
           frame = frame + "&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=js&rawInputLstJSON=%5B%5D&textReferences=false\"> </iframe>";
           $('#visual-area').html(frame);
       }

       else{
           alert('Feature Not Supported Yet For '+$('#selectLanguageBtn').find('option:selected').html());
       }
   });
});
