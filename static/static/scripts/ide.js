var BASE_URL = "http://172.16.10.48:3000";
var BASE_URI = "http://127.0.0.1:5050";

var sourceEditor, inputEditor, outputEditor;
var $insertTemplateBtn, $selectLanguageBtn, $runBtn ,$submission,$problem,$problem_number,$problem_id;
var $statusLine, $emptyIndicator,$testdata;
var box='';


function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}


function problempopup() {
  var newWindow = window.open();
  newWindow.document.write(document.getElementById("hide").innerHTML);
}

function updateEmptyIndicator() {
  if (outputEditor.getValue() === "") {
    $emptyIndicator.html("empty");
  } else {
    $emptyIndicator.html("");
  }
}

function handleError(jqXHR, textStatus, errorThrown) {
  outputEditor.setValue(JSON.stringify(jqXHR, null, 4));
  $statusLine.html(`${jqXHR.statusText} (${jqXHR.status})`);
  $runBtn.button("reset");
  updateEmptyIndicator();
}

function run() {
  if (sourceEditor.getValue().trim() === "") {
    alert("Source code can't be empty.");
    return;
  } else {
    $runBtn.button("loading");
  }
if($("#testcasecheck").is(':checked'))
{ var sourceValue = (sourceEditor.getValue());
  var inputValue = (inputEditor.getValue());
  var languageId = $selectLanguageBtn.val();
  var data = {
    source_code: sourceValue,
    language_id: languageId,
    input: inputValue,
   // expected_output:""
  };
  $.ajax({
    url: BASE_URL + "/submissions?base64_encoded=false",
    type: "POST",
    async: true,
    contentType: "application/json",
    data: JSON.stringify(data),
    success: function(data, textStatus, jqXHR) {
      console.log(`Your submission token is: ${data.token}`);
      setTimeout(fetchSubmission.bind(null, data.token), 1500);
    },
    error: handleError
  });

}

else{
  $testdata=JSON.parse($("#testCaseData").attr("data"));

for(i=2;i<Object.keys($testdata).length-2;i=i+2)
{
  var sourceValue = (sourceEditor.getValue());
  var inputValue = $testdata[i].value;
  var languageId = $selectLanguageBtn.val();
  var expected_output=$testdata[i+1].value;
  var data = {
    source_code: sourceValue,
    language_id: languageId,
    input: inputValue,
    expected_output:expected_output
  };

var num=i/2;
(function(num){
$.ajax({
    url: BASE_URL + "/submissions?base64_encoded=false",
    type: "POST",
    async: true,
    contentType: "application/json",
    data: JSON.stringify(data),
    success: function(data, textStatus, jqXHR) {
      console.log(`Your submission token is: ${data.token}`);
      setTimeout(fetchSubmission.bind(null, data.token,num), 1500);
    },
    error: handleError
  });
})(num);
}
}

};
function review() {
  if (sourceEditor.getValue().trim() === "") {  //submission
    alert("Source code can't be empty.");
    return;
  }

  var sourceValue = (sourceEditor.getValue());
  var inputValue = (inputEditor.getValue());
  var languageId = $selectLanguageBtn.val();
  var problem_statement=$.trim($problem.html());
  var problem_Number = $problem_number.html();
  var problem_Id = $problem_id.html();
  console.log(problem_Number);
  console.log(problem_Id);
 // var expected_output="hello, world"
  var data = {
    source_code: sourceValue,
    problem: problem_statement,
    problem_number: problem_Number,
    problem_id: problem_Id
    };

console.log(BASE_URI +"/review");

  $.ajax({
    url: "/review",   //BASE_URI +
    type: "POST",
    async: true,
    contentType: "application/json",
    data: JSON.stringify(data),

    success: function(data, textStatus, jqXHR) {
    },
    error: handleError
  });
alert("Request has been made for review \n Go back and refresh the page to check status.");
};

function fetchSubmission(submission_token,num) {
  $.ajax({
    url: BASE_URL + "/submissions/" + submission_token + "?base64_encoded=true",
    type: "GET",
    async: true,

    success: function(data, textStatus, jqXHR) {
      if (data.status.id <= 2) {

        setTimeout(fetchSubmission.bind(null, submission_token,num), 1000);
        return;
      }
      if(data.status.description=='Wrong Answer')
      { var imgbox="#imgbox"+num;
        var testbox="#testbox"+num;
        $(imgbox).remove();
        $(testbox).append('<img id="imgbox'+num+'" src="/static/images/cross.png" style="width:15px;height:15px;float:left;">');
      }
      if(data.status.description=='Accepted')
      { var imgbox="#imgbox"+num;
        var testbox="#testbox"+num;
        $(imgbox).remove();
        $(testbox).append('<img id="imgbox'+num+'" src="/static/images/tick.png" style="width:15px;height:15px;float:left;">');
      }
      var status = data.status;
      var stdout = atob(data.stdout || "");
      var stderr = atob(data.stderr || "");
      var time = (data.time === null ? "-" : data.time + "s");
      var memory = (data.memory === null ? "-" : data.memory + "KB");

      $statusLine.html('${status.description}, ${time}, ${memory}');

      if (status.id !== 3 && stderr !== "") { // If status is not "Accepted", merge stdout and stderr
        stdout += (stdout === "" ? "" : "\n") + stderr;
      }
      outputEditor.setValue(stdout);

      updateEmptyIndicator();
      $runBtn.button("reset");
    },
    error: handleError
  });
}

function setEditorMode() {
  sourceEditor.setOption("mode", $selectLanguageBtn.find(":selected").attr("mode"));
}

function insertTemplate() {
  var value = parseInt($selectLanguageBtn.val());
  sourceEditor.setValue(sources[value]);
  sourceEditor.focus();
  sourceEditor.setCursor(sourceEditor.lineCount(), 0);
}

$(document).ready(function() {

  console.log("Shinobis, Have fun");
  if (window.location.protocol === "file:") {
    BASE_URL = "http://localhost:3000"; // If running locally, you probably couldn't use any other API except localhost
  }

  $selectLanguageBtn = $("#selectLanguageBtn");
  $insertTemplateBtn = $("#insertTemplateBtn");
  $runBtn = $("#runBtn");
  $submission=$("#submission");
  $problem=$("#problem");
  $problem_number=$("#problem_number");
  $problem_id=$("#problem_id");
  $emptyIndicator = $("#emptyIndicator");
  $statusLine = $("#statusLine");

  sourceEditor = CodeMirror(document.getElementById("sourceEditor"), {
    lineNumbers: true,
    indentUnit: 4,
  });
  var randomChildIndex =18;
  $selectLanguageBtn[0][randomChildIndex].selected = true;
  setEditorMode();
  insertTemplate();

  inputEditor = CodeMirror(document.getElementById("inputEditor"), {
    lineNumbers: true,
    mode: "plain"
  });
  outputEditor = CodeMirror(document.getElementById("outputEditor"), {
    readOnly: true,
    mode: "plain"
  });

  $("#testcasecheck").click(function(){
  if(this.checked){
    $( "#problemtestcases" ).remove();
    $("#area").append('  <div id="customtest"class="container-fluid" ><div class="row labels"><div id="inputLabel" class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><h4><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Input<small> optional</small></h4></div><div id="outputLabel" class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><h4><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Output <small id="emptyIndicator">empty</small><small class="pull-right" id="statusLine"></small></h4></div></div><div class="row ioEditors"><div id="inputEditor" class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div><div id="outputEditor" class="col-xs-6 col-sm-6 col-md-6 col-lg-6"></div></div></div>');
    inputEditor = CodeMirror(document.getElementById("inputEditor"), {
      lineNumbers: true,
      mode: "plain"
    });
    outputEditor = CodeMirror(document.getElementById("outputEditor"), {
      readOnly: true,
      mode: "plain"
    });
  }
  else{
    $( "#customtest" ).remove();
    $("#area").append('<div class="container-fluid" id="problemtestcases" >'+box+'</div>');

  }
});
  $selectLanguageBtn.change(function(e) {
    setEditorMode();
  });

  $insertTemplateBtn.click(function(e) {
    insertTemplate();
  });

  $("body").keydown(function(e){
    var keyCode = e.keyCode || e.which;
    if (keyCode == 120) { // F9
      e.preventDefault();
      run();
    }
   else if(keyCode == 123){
   //  e.preventDefault();
   }
  });

  $runBtn.click(function(e) {
    run();
  });
  $submission.click(function(e){

review();
window.close();
});
if(($('#code').attr('data-code'))!=""){
  sourceEditor.setValue(($('#code').attr('data-code')));
}
});

// Template Sources
var bashSource = "echo \"hello, world\"\n";

var cSource = "\
#include <stdio.h>\n\
\n\
int main(void) {\n\
    printf(\"hello, world\\n\");\n\
    return 0;\n\
}\n";

var cppSource = "\
#include <iostream>\n\
\n\
int main() {\n\
    std::cout << \"hello, world\" << std::endl;\n\
    return 0;\n\
}\n";

var csharpSource = "\
public class Hello {\n\
    public static void Main() {\n\
        System.Console.WriteLine(\"hello, world\");\n\
     }\n\
}\n";

var haskellSource = "main = putStrLn \"hello, world\"\n";

var javaSource = "\
public class Main {\n\
  public static void main(String[] args) {\n\
    System.out.println(\"hello, world\");\n\
  }\n\
}\n";

var octaveSource = "printf(\"hello, world\\n\");\n";

var pascalSource = "\
program Hello;\n\
begin\n\
  writeln ('hello, world')\n\
end.\n";

var pythonSource = "print(\"hello, world\")\n";

var rubySource = "puts \"hello, world\"\n";

var javaScriptSource = "console.log(\"hello, world\");";

var sources = {
  1: bashSource,
  2: bashSource,
  3: cSource,
  4: cSource,
  5: cSource,
  6: cSource,
  7: cppSource,
  8: cppSource,
  9: cppSource,
  10: cppSource,
  11: csharpSource,
  12: haskellSource,
  13: javaSource,
  14: javaSource,
  15: octaveSource,
  16: pascalSource,
  17: pythonSource,
  18: pythonSource,
  19: pythonSource,
  20: pythonSource,
  21: rubySource,
  22: rubySource,
  23: rubySource,
  24: rubySource,
  25: javaScriptSource,
  26: javaScriptSource
};
