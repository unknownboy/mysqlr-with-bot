var Router= require('koa-router');
var bodyParser = require('koa-body')();
var formParser = require('koa-body')({
    formidable: {uploadDir: './static/static/uploads'},
    multipart: true,
    urlencoded: true
});
module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    var terminalCtrl = require('./../controllers/terminalCtrl');
    var labCtrl = require('./../controllers/labCtrl');
    var botCtrl = require('./../controllers/botCtrl');



    router.get('/signup', welcomeCtrl.showSignupPage); //used
    router.get('/login', welcomeCtrl.showHomePage);  //used
    router.post('/signup', bodyParser, welcomeCtrl.signup); //used
    router.post('/login', bodyParser, welcomeCtrl.login); //used
    router.get('/dashboard',welcomeCtrl.showDashboard); //used
    router.get('/labs',welcomeCtrl.showLabs); //used
    router.get('/profile',welcomeCtrl.showProfile); //used
    router.get('/student',welcomeCtrl.showStudents); //used
    router.get('/grade',welcomeCtrl.showGrade); //used
    router.post('/edit_profile',bodyParser,welcomeCtrl.editProfile); //used
    router.post('/addnewlab',bodyParser,welcomeCtrl.addnewLab); //used
    router.get('/practicals/:pid',welcomeCtrl.showPractical); //used
    router.post('/addnewpractical',formParser,welcomeCtrl.addnewpractical); //used
    router.get('/problems/:pid',welcomeCtrl.showProblems); //used
    router.get('/addproblem/:pid',labCtrl.addproblem);//used
    router.post('/addproblem',bodyParser,welcomeCtrl.addproblem2); //used
    router.get('/labrequest',welcomeCtrl.labrequest); //used
    router.post('/requestnewlab',bodyParser,welcomeCtrl.requestnewlab); //used
    router.post('/labaccept',bodyParser,welcomeCtrl.labaccept); //used
    router.post('/labreject',bodyParser,welcomeCtrl.labreject); //used
    router.post('/showp',bodyParser,welcomeCtrl.showp); //used
    router.post('/deletep',bodyParser,welcomeCtrl.deletep); //used
    router.post('/hidep',bodyParser,welcomeCtrl.hidep); //used
    router.post('/dropdb',bodyParser,welcomeCtrl.dropdb); //used
    router.get('/mydumps',welcomeCtrl.mydumps); //used
    router.get('/terminal',terminalCtrl.terminal); //used
    router.post('/doQuery',bodyParser,terminalCtrl.doQuery); //used
    router.get('/terminal/:dump',terminalCtrl.terminalDump); //used
    router.get('/students/:sid',welcomeCtrl.studentStat); //used
    router.get('/labstudents/:lid',welcomeCtrl.labstudents); //used
    router.get('/pstatus/:qid',welcomeCtrl.showPStatus); //used
    router.get('/bot', botCtrl.showHomePage);
    router.get('/go',botCtrl.go);
    router.post('/go',bodyParser,botCtrl.go);
    router.get('/temp',terminalCtrl.tempfunc);
    router.get('/', welcomeCtrl.showLandingPage);





    router.get('/forgot_password', welcomeCtrl.showforgotpass);
    router.post('/forgotpass',bodyParser, welcomeCtrl.forgotpass);






    router.get('/home', welcomeCtrl.showHomePage);

    router.get('/logout', welcomeCtrl.logout);
    router.get('/uinfo', welcomeCtrl.uinfo);
    router.post('/request-form',bodyParser,welcomeCtrl.requestForm);

    //route for coding lab
    router.post('/solvequery',bodyParser,welcomeCtrl.ide);

    //route for noncoding lab
    router.post('/noncodinglab',bodyParser,welcomeCtrl.opennoncodinglab);
    router.post('/noncodinglabcontent',bodyParser,welcomeCtrl.showNonCodingData);







    router.post('/review',bodyParser, welcomeCtrl.review);
    router.get('/facultylab',welcomeCtrl.facultylab);
    router.get('/admin',welcomeCtrl.admin);
    router.get('/recovery/:hash',welcomeCtrl.changepass);
    router.post('/updatepass', bodyParser, welcomeCtrl.updatepass);
    router.post('/reviewcode',bodyParser,welcomeCtrl.reviewcode);

    router.post('/admintask',bodyParser,welcomeCtrl.admintask);
    router.post('/admintasklab',bodyParser,welcomeCtrl.admintasklab);

   // router.get('/:lab_name/sec/:sec',)

    //lab Routes



    //route for adding lab
    router.get('/addlab',labCtrl.addLab);
    router.post('/addlab',bodyParser,labCtrl.addLabProcedure);
    //route for listing all request with lab_id as query parameter

    router.get('/request',labCtrl.showLabRequests);



    router.get('/lab/:lab_name', labCtrl.viewproblem);
    router.get('/:lab_name/sec/:section',labCtrl.facultyprob);
    router.get('/:lab_name/addproblem/:section',labCtrl.addproblem);
    router.post('/addprobindb',bodyParser,labCtrl.addprobindb);
    router.post('/deleteproblem',bodyParser,labCtrl.deleteproblem);
    router.post('/studentstatus',bodyParser,labCtrl.probstudent);
    router.post('/userapproval',bodyParser,labCtrl.approve);
    router.post('/restoreproblem',bodyParser,labCtrl.restoreproblem);
    router.post('/deleteproblempermanently',bodyParser,labCtrl.deleteproblempermanently);
    router.post('/studentlist', bodyParser, labCtrl.studentslistaccordingtolab);
    router.post('/problemlistofstudent',bodyParser, labCtrl.problemlistofstudent);
    router.post('/rejectcode',bodyParser,labCtrl.rejectcode); 
    //router.get('/*',welcomeCtrl.showHomePage);
    router.get('/edit_profile',welcomeCtrl.editProfilePage);






    //api routes
    var apiCtrl = require('./../controllers/apiCtrl');
    router.post('/addcomment',bodyParser,apiCtrl.addcomment);
    router.post('/getcomments',bodyParser,apiCtrl.getcomments);
    router.post('/getlabname',bodyParser,apiCtrl.getlabname);
    router.post('/enroll',bodyParser,apiCtrl.enroll);

    router.post('/getallreq',bodyParser,apiCtrl.getallRequests);

    router.post('/grantpermission',bodyParser,apiCtrl.grantrequest);
    router.post('/cancelpermission',bodyParser,apiCtrl.cancelrequest);
    router.post('/saveNonCodingContent',bodyParser,apiCtrl.saveNonCodingContent);






    // router.post('/tt',bodyParser,apiCtrl.tt);

    return router.middleware();
}
